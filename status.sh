#!/bin/bash
filename="/home/bblite/subscribers/subscribers_for_`date -d '1 day ago' +'%a_%d-%m-%Y'`.csv"
query=$(sqlplus bblite/bblite@10.93.85.85/bblite <<EOF
set colsep ,     -- separate columns with a comma
set pagesize 0   -- only one header row
set trimspool on -- remove trailing blanks
set headsep off  -- this may or may not be useful...depends on your headings.
set linesize 250
spool $filename
select * from subscriber;
exit;
EOF)

