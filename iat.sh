#!/bin/bash

function res() {
	cd /home/bblite/$1/bin
	echo "Shutting down $1" 
	./shutdown.sh
	sleep 5
	if [[ $1 = "SDP-WS-cfx" ]]
	then PID=`ps -ef | grep SDP-[W] | awk '{print $2}'`
	elif [[ $1 = "SDP-server" ]]
	then PID=`ps -ef | grep SDP-[s] | awk '{print $2}'`
	echo $PID
	elif [[ $1 = "SDP-Scheduler-server" ]]
	then PID=`ps -ef | grep SDP-[S] | awk '{print $2}'`
	fi

	if [[ -n $PID ]]
		then kill -9 $PID
		sleep 5
	fi
	echo "Starting $1"
	./startup.sh
}

function implement(){
	echo "Moving PreIAT configurations"
	cp /home/bblite/SDP-WS-cfx/webapps/bbspring/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties /home/bblite/SDP-WS-cfx/webapps/bbspring/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties_postiat
	cp /home/bblite/SDP-WS-cfx/webapps/bbspring/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties_preiat /home/bblite/SDP-WS-cfx/webapps/bbspring/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties
  res SDP-WS-cfx
  cp /home/bblite/SDP-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties /home/bblite/SDP-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties_postiat
  cp /home/bblite/SDP-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties_preiat /home/bblite/SDP-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties
  res SDP-server
  cp /home/bblite/SDP-Scheduler-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties /home/bblite/SDP-Scheduler-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties_postiat
  cp /home/bblite/SDP-Scheduler-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties_preiat /home/bblite/SDP-Scheduler-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties
  res SDP-Scheduler-server

}


function rollback(){
	echo "Rolling back IAT configurations"
	cp /home/bblite/SDP-WS-cfx/webapps/bbspring/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties_postiat /home/bblite/SDP-WS-cfx/webapps/bbspring/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties
  res SDP-WS-cfx
  cp /home/bblite/SDP-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties_postiat /home/bblite/SDP-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties
  res SDP-server
  cp /home/bblite/SDP-Scheduler-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties_postiat /home/bblite/SDP-Scheduler-server/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties
  res SDP-Scheduler-server

}

while [[ $1 = -* ]]; do
    arg=$1; shift           # shift the found arg away.

    case $arg in
        --implement)
            implement
            #shift           # foo takes an arg, needs an extra shift
            ;;
        --rollback)
            rollback          # bar takes no arg, doesn't need an extra shift
            ;;
    esac
done


