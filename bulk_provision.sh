#!/bin/bash

#This script should be used for provisioning of subscribers from the backend

#Replace the value of FILENAME with absolute path to file containing the MSISDNS to be provisioned
FILENAME=/home/bblite/msisdn.txt
#Replace the value of BROKER_URL with the URL to the BB Broker, ensure it includes the port it is starting on
BROKER_URL="http://127.0.0.1:8017/blackberry/smsservice"
#Replace the value of SHORTCODE_STRING with the SHORTCODE you aim to provision 

for i in `cat $FILENAME`
do 
  echo "Processing $i"
  MSISDN=`echo $i | cut -d ',' -f1`
  SHORTCODE_STRING=`echo $i | cut -d ',' -f2`
  URL="$BROKER_URL?msisdn=$MSISDN&msg=$SHORTCODE_STRING"
  echo $URL
  curl -v $URL
done

