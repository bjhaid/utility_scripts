#!/bin/bash
function rotate {
  fname=$1
  value=200000000 #File maximum accepted value
  FILESIZE=`ls -l $fname | awk '{print $5}'`
  #filesize in number
  size=`echo $FILESIZE | sed -r 's/M|G|K//'`

  if [[ ${size%%.*} -ge $value ]]
  then 
  echo $fname 
  echo > $fname
  else
  echo "$fname is smaller than 200MB"
  fi
}
#Specify files to be truncated in the format rotate absolute_path to file e.g:
#rotate /home/bblite/SDP-server/logs/catalina.out 
#the user requires permission to the file to be truncated
rotate /home/bblite/SDP-server/logs/catalina.out
rotate /home/bblite/SDP-WS-cfx/logs/catalina.out
rotate /home/bblite/SDP-Scheduler-server/logs/catalina.out


