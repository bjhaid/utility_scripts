Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('CC2957A329768E47df401BAC1402281E','bbcom90prep','90Days COMPLETE PLAN',90,null,'30000','Prepaid Complete',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('e1616f5c89aeb329bffaa0ec4ce04242','bbsoc90prep','Blackberry Complete Three Months Social Plan',90,'0','36000','Prepaid Social Plan',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('dc3ae9e040fe366ecc11a4c6fab87b7e','bb30prep','Blackberry BIS Monthly Service',30,'0','30000','Prepaid Prosumer',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('e0616f5c89aeb329231a0ec4ce04242','bbcom7prep','Blackberry Complete Weekly Plan',7,'0','5000','Prepaid Complete',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('019fd37f965befaac7a5f4d6c193a4','bb7prep','Blackberry BIS Weekly Service',7,'0','10000','Prepaid Prosumer',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('34b77aeffc3c2c4bbed537d9ad6c32c9','bb1prep','Blackberry BIS Daily Service',1,'0','1800','Prepaid Prosumer',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('9bfcfe5ba198124f2ff327e61d3c9a9','besmonthprep','Blackberry BES Monthly Service',30,'0','35000','Enterprise Plus',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('3e618a2c60cda419221a0dd4ce07640','bbsoc1prep','Blackberry Complete Daily Social Plan',1,'0','700','Prepaid Social Plan',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('4e618a2c60cda41ce30a0dd4ce07640','bbsoc7prep','Blackberry Complete Weekly Social Plan',7,'0','3500','Prepaid Social Plan',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('5e618a2c60cda4147d0a0dd4ce07640','bbsoc30prep','Blackberry Complete Monthly Social Plan',30,'0','12000','Prepaid Social Plan',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('e0616f5c89aeb33250a0ec4ce04242','bbcom7prep','Blackberry Complete Weekly Plan',7,'0','5000','Prepaid Complete',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('3e618a2c60cda20d3e0a0dd4ce07641','bbcom30prep','Blackberry Complete Monthly Plan',30,'0','17500','Prepaid Complete',0,1);
Insert into BILLINGPLAN (ID,SHORTCODE,DESCRIPTION,VALIDITY,DAVALUE,COST,SERVICES,FIRST_MONTH_FREE,NUMBER_OF_DAYS) values ('3e618a2c60cda218870a0dd4ce07641','bbcom1prep','Blackberry Complete Daily Plan',1,'0','1000','Prepaid Complete',0,1);

  CREATE TABLE "BBLITE"."USERS" 
   (	"ID" VARCHAR2(36 BYTE), 
	"USERNAME" VARCHAR2(50 BYTE), 
	"PASSWORD" VARCHAR2(50 BYTE), 
	"FIRSTNAME" VARCHAR2(50 BYTE), 
	"SURNAME" VARCHAR2(50 BYTE), 
	"ADMINID" VARCHAR2(50 BYTE), 
	"FLAG" VARCHAR2(50 BYTE), 
	"DATECREATED" DATE, 
	"USERTYPE" VARCHAR2(50 BYTE), 
	"TELEPHONE" VARCHAR2(50 BYTE), 
	"EMAIL" VARCHAR2(50 BYTE)
   );

  ALTER TABLE "BBLITE"."SUBSCRIBER"
    ADD(
	"PREPAIDSUBSCRIBER" NUMBER(1,0)  DEFAULT (1), 
	"POSTPAIDSUBSCRIBER" NUMBER(1,0) DEFAULT (0), 
	"SHORTCODE" VARCHAR2(35 BYTE));

  ALTER TABLE "BBLITE"."BILLINGPLAN"
    ADD(
	"STATUSID" VARCHAR2(35 BYTE) DEFAULT ('1')
    );

  CREATE TABLE "BBLITE"."REQUESTTRACKER" 
   (	"ID" VARCHAR2(36 BYTE), 
	"MSISDN" VARCHAR2(12 BYTE),  
	"SHORTCODE" VARCHAR2(35 BYTE), 
	"DATE_CREATED" DATE
   );

  ALTER TABLE "BBLITE"."TRANSACTIONLOG"
    ADD(
	"SERVICE" VARCHAR2(30 BYTE), 
	"STATUS" VARCHAR2(20 BYTE));

  CREATE TABLE "BBLITE"."ERRORLOG" 
   (	"ID" VARCHAR2(36 BYTE), 
	"DESCRIPTION" VARCHAR2(255 BYTE), 
	"MSISDN" VARCHAR2(100 BYTE), 
	"DATE_CREATED" DATE, 
	"ERRORTYPE" VARCHAR2(50 BYTE), 
	"ERRORCODE" VARCHAR2(50 BYTE)
   );

  CREATE TABLE "BBLITE"."PASSWORDLOG" 
   (	"ID" VARCHAR2(250 BYTE), 
	"USERNAME" VARCHAR2(250 BYTE), 
	"PASSWORD" VARCHAR2(250 BYTE), 
	"USERTYPE" VARCHAR2(250 BYTE), 
	"FLAG" VARCHAR2(25 BYTE), 
	"PARENTID" VARCHAR2(20 BYTE), 
	"DATECREATED" DATE, 
	"EXTRA" VARCHAR2(50 BYTE)
   );

  CREATE TABLE "BBLITE"."ACTIVITYLOGGER" 
   (	"ID" VARCHAR2(36 BYTE), 
	"DESCRIPTION" VARCHAR2(255 BYTE), 
	"MSISDN" VARCHAR2(12 BYTE), 
	"ACTION" VARCHAR2(50 BYTE), 
	"DATE_CREATED" DATE, 
	"USERNAME" VARCHAR2(50 BYTE), 
	"HOST_IP" VARCHAR2(50 BYTE)
   );
commit;

select billingplan.shortcode, subscriber.shortcode from subscriber inner join billingplan on subscriber.serviceplanid = billingplan.services where subscriber.servicetype = billingplan.validity;

select billingplan.shortcode from billingplan where subscriber.serviceplanid = billingplan.services and subscriber.servicetype = billingplan.validity and rownum <= 1;
update subscriber sub set sub.shortcode = (select billingplan.shortcode from billingplan where sub.serviceplanid = billingplan.services and sub.servicetype = billingplan.validity and rownum <= 1)
where sub.shortcode is null
and exists
(select billingplan.shortcode from billingplan where sub.serviceplanid = billingplan.services and sub.servicetype = billingplan.validity and rownum <= 1);

update subscriber set prepaidsubscriber = 1, postpaidsubscriber = 0;

alter table subscriber rename column auto_renew to autorenew;

commit;