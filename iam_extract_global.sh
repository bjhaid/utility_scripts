#!/bin/bash

#===============================================================================
# Version Date         # Author             Description
#===============================================================================
# V1.0    2012-02-10   # Vladislav Tembekov Ported from ksh to bash
# V1.1    2012-04-13   # Vladislav Tembekov Fixed extraction GSA users from sudoers file issue
# V1.1.1  2012-04-13   # Vladislav Tembekov Fixed incorrect priv group assignment
# V1.1.2  2012-06-18   # Vladislav Tembekov Fixed incorrect SUDO_ALL assignment
# V1.1.3  2012-07-24   # Vladislav Tembekov Added labeling LDAP IDS in autoldap mode
# V1.1.4  2012-09-06   # Vladislav Tembekov Added check user state on tru64
# V1.1.5  2012-09-20   # Vladislav Tembekov Added -l switch
# V1.1.6  2012-09-28   # Vladislav Tembekov Added check belonging user to a group 
# V1.1.7  2012-10-16   # Vladislav Tembekov Improved check user state on AIX
# V1.1.8  2012-10-30   # Vladislav Tembekov Change GSA config check condition
# V1.1.9  2012-11-16   # Vladislav Tembekov Fixed parsing sudoers include directive
# V1.2.0  2012-11-21   # Vladislav Tembekov Added filter of NIS users
#===============================================================================

VERSION="V1.2.0"

################################################################################
SIG=""
HOST=""
FQDN=0
DEBUG=0
EXIT_CODE=0
OUTPUTFILE=""
KNOWPAR=""
UNKNOWPAR=""
#################################################################################
function logMsg
{
  level=$1
  msg=$2
  echo "[$level] $msg"
}

function logDiv
{
  logMsg "INFO" "==========================================="
}

function logAbort
{
  logMsg "ERROR" "$1"
  EXIT_CODE=9
  logFooter
  exit 9
}

function logDebug
{
  if [[ $DEBUG -ne 0 ]]; then
    logMsg "DEBUG" "$1"
  fi
}

function logInfo
{
  logMsg "INFO" "$1"
}

function logMsgVerNotSupp
{
  logMsg "ERROR" "The found version of the Sub System is not supported by the given script."
}

function logHeader
{
  STARTTIME=`date +%Y-%m-%d-%H.%M.%S`
  
  logInfo "UID EXTRACTOR EXECUTION - Started"
  logInfo "START TIME: $STARTTIME"
  logDiv
  logInfo "IAM Global OS Extractor"
  logDiv
}

function logPostHeader
{
  if [[ $KNOWPAR != "" ]]; then
    logInfo "Following parameters will be processed: $KNOWPAR"
  fi
  
  if [[ $UNKNOWPAR != "" ]]; then
    logMsg "WARN" "Following unknown parameters will not be processed: $UNKNOWPAR"
  fi
  
  logDiv
  logInfo "SCRIPT NAME: ${1#./}"
  logInfo "SCRIPT VERSION: $VERSION"
  logInfo "CKSUM: $CKSUM"
  logInfo "OS CAPTION: `uname`"
  logInfo "OS VERSION: `uname -r`"
  logInfo "HOSTNAME: $HOSTNAME"
  logInfo "CUSTOMER: $CUSTOMER"
  logInfo "OUTPUTFILE: $OUTPUTFILE"
  logInfo "SIGNATURE: $SIG"
  
  logInfo "IS_AG: no"
  logInfo "IS_ALLUSERIDS: yes"
  
  if [ $FQDN -ne 0 ]; then
    logInfo "IS_FQDN: yes"
  else
    logInfo "IS_FQDN: no"
  fi

  if [ $DEBUG -ne 0 ]; then
    logInfo "IS_DEBUG: yes"
  else
    logInfo "IS_DEBUG: no"
  fi
  
  logDiv
  
  logInfo "EXTRACTION PROCESS - Started"
  if [ $DEBUG -ne 0 ]; then
    logDiv
  fi
}

function logFooter
{
  if [ $DEBUG -ne 0 ]; then
    logDiv
  fi
  
  logInfo "EXTRACTION PROCESS - Finished"
  logDiv
  if [[ $EXIT_CODE -lt 2 ]]; then
    logInfo "The mef3 data has been collected"
  else
    logInfo "The mef3 data has not been collected"
    `rm -f $OUTPUTFILE`
  fi
  logDiv
  logInfo "Time elapsed: `echo $SECONDS`"
  logDiv
  
  if [[ $EXIT_CODE -lt 2 ]]; then
    logInfo "The report has been finished with success" 
  else
    logInfo "The report has been finished without success" 
  fi
    
  logInfo "General return code: $EXIT_CODE"
  logInfo "UID EXTRACTOR EXECUTION - Finished"
}
#####################################################################################

### Start of AssocArr lib
# Associative array routines
# @(#) AssocArr 1.5
# 1993-06-25 john h. dubois iii (john@armory.com)
# 1993-07-09 Changed syntax of AStore so that these functions can be used
#            for set operations.
# 1994-06-26 Added append capability to AStore
# 1995-10-19 Keep track of highest element used, and pass it to Ind
# 2000-11-26 Added m_AStore and APrintAll
# 2001-06-24 Avoid some evals by using (()) to dereference integer var names.
# 2001-07-14 Fixed bug in AStore
# 2002-01-30 Fixed bugs in AGet and ADelete
# 2002-02-03 Added ANElem
# 2002-11-14 ksh93 compatibility fix
# 2003-07-27 1.5 Added AInit
#
# These routines use two shell arrays and an integer variable for each
# associative array:
# For associative array "foo", the values are stored in foo_val[1..255] and the
# indices (free form character strings) are stored in foo_ind[].
# The free pointer is stored in foo_free.  It has the value of the lowest index
# that may be free. The end pointer is stored in foo_end; it has the value of
# the highest index used.
# Only 255 values can be stored.
# Arrays must have names that are valid shell variable names.
# A null array index is not allowed.

# Usage: Ind <arrayname> <value> [[<nsearch>] <firstelem>]
# Returns the index of the first element of <arrayname> that has value <value>.
# Note, <arrayname> is a full ksh array name, not an associate array name as
# used by this library.
# Returns 0 if it is none found.
# Works only for indexes 1..255.
# If <nsearch> is given, the first <nsearch> elements of the array are
# searched, with only nonempty elements counted.
# If not, the first n nonempty elements are searched,
# where n is the number of elements in the array.
# If a fourth argument (<firstelem>) is given, it is the index to start with;
# the search continues for <nsearch> elements.
# Element zero should not be set.
function Ind
{
  declare -i NElem ElemNum=${5:-1} NumNonNull=0 num_set
  declare Arr=$1 Val=$2 Res=$3 ElemVal

  eval num_set=\${#$Arr[*]}
    if [[ $# -eq 4 ]]; then
      NElem=$4
      # No point in searching more elements than are set
      (( NElem > num_set )) && NElem=num_set
    else
    NElem=$num_set
  fi
  while (( ElemNum <= 99999 && NumNonNull < NElem )); do
    eval ElemVal=\"\${$Arr[ElemNum]}\"
    if [[ $Val = $ElemVal ]]; then
      eval ${Res}=$ElemNum
    return 1
  fi
  [[ -n $ElemVal ]] && ((NumNonNull+=1))
  ((ElemNum+=1))
  done
  return 0
}

# Usage: AInit <arrayname> <index1> <value1> [<index2> <value2>] ...
# Stores each value in associative array <arrayname> under the associated
# index.  Up to 255 index/value pairs may be given.
# <arrayname> is treated as though it is initially empty.
# Return value is 0 for success, 1 for failure due to full array,
# 2 for failure due to bad index or arrayname, 3 for bad syntax
function AInit
{
  declare Arr=$1
  declare -i Ind

  shift
  # Arr must be a valid ksh variable name
  #[[ $Arr != [[:alpha:]_]*([[:word:]]) ]] && return 2
  (( $# % 2 != 0 )) && return 3

  Ind=1
  while (( $# > 0 && Ind < 100000 )); do
    Index=$1
    Val=$2
  [[ -z $Index ]] && return 2
  eval ${Arr}_ind[Ind]=\$Index ${Arr}_val[Ind]=\$Val
  ((Ind+=1))
  shift 2
  done
  (( ${Arr}_free=Ind ))
  (( ${Arr}_end=Ind-1 ))
  (( $# > 0 )) && return 1
  return 0
}

# Usage: AStore <arrayname> <index> [<value> [<append>]]
# Stores value <value> in associative array <arrayname> with index <index>
# If no <value> is given, nothing is stored in the value array.
# This can be used for set operations.
# If a 4th argument is given, the value is appended to the current value
# stored for the index (if any).
# Return value is 0 for success, 1 for failure due to full array,
# 2 for failure due to bad index or arrayname, 3 for bad syntax
function AStore
{
  declare Arr=$1 Index=$2 Val=$3
  declare -i Used Free=0 NumArgs=$# arrEnd
  NumInd=0
  [[ -z $Index ]] && return 2
  # Arr must be a valid ksh variable name
  #    [[ $Arr != [[:alpha:]_]*([[:word:]]) ]] && return 2

  if eval [[ -z \"\$${Arr}_free\" ]]; then      # New array
    # Start free pointer at 1 - we do not use element 0
    Free=1
    arrEnd=0
    NumInd=0
  else  # Extant array
    (( arrEnd=${Arr}_end ))
    Ind ${Arr}_ind "$Index" NumInd $arrEnd
  fi
  # If the supplied <index> is not in use yet, we must find a slot for it
  # and store the index in that slot.
  if [[ NumInd -eq 0 ]]; then
    if [[ Free -eq 0 ]]; then # If this is not a newly created array...
      eval Used=\${#${Arr}_ind[*]}
      if [[ Used -eq 99999 ]]; then
        logMsg "ERROR" "Adding $Val to Array:$Arr is FULL: $Used of 99999"
        EXIT_CODE=1
        return 1 # No space available
      fi
      (( Free=${Arr}_free ))
    fi
    # Find an unused element
    while eval [[ -n \"\${${Arr}_ind[Free]}\" ]]; do
      ((Free+=1))
      (( Free > 99999 )) && Free=1  # wrap
    done
    NumInd=Free
    (( NumInd > arrEnd )) && arrEnd=NumInd
    (( ${Arr}_free=Free ))
    (( ${Arr}_end=$arrEnd ))
    # Store index
    eval ${Arr}_ind[NumInd]=\$Index
  fi
  case $NumArgs in
    2) return 0;;     # Set no value
    3) eval ${Arr}_val[NumInd]=\$Val;;  # Store value
    4)  # Append value
      eval ${Arr}_val[NumInd]=\"\${${Arr}_val[NumInd]}\$Val\";;
    *) return 3;;
  esac
  return 0
}

# Usage: m_AStore <arrayname> <append> <index> <value> [<index> <value> ...]
# Stores multiple values in associative array <arrayname>.
# For each <index>,<value> pair, <value> is stored under the index <index>
# in associate array <arrayname>.
# If <append> is non-null, values are appended to current values
# stored for indexes (if any).
# See AStore for details.
# On success, 0 is returned.
# If an error occurs, array insertion stops and the error returned by
# AStore is returned.
function m_AStore
{
  declare Arr=$1 Append=$2

  shift 2
  while (( $# > 0 )); do
    AStore "$Arr" "$1" "$2" $Append || return $?
    shift 2
  done
  return 0
}

# Usage: AGet <arrayname> <index> <var>
# Finds the value indexed by <index> in associative array <arrayname>.
# If there is no such array or index, 0 is returned and <var> is not touched.
# Otherwise, <var> (if given) is set to the indexed value and the numeric index
# for <index> in the arrays is returned.
function AGet
{
  declare Arr=$1 Index=$2 Var=$3 End
  NumInd=0
  # Can't use implicit integer referencing on ${Arr}_end here because it may
  # not be set yet.
  eval End=\$${Arr}_end
  [[ -z $End ]] && return 0

  Ind ${Arr}_ind "$Index" NumInd $End
  if (( NumInd > 0 )) && [[ -n $Var ]]; then
    eval $Var=\"\${${Arr}_val[NumInd]}\"
  fi
  return $NumInd
}

# Usage: AUnset <arrayname>
# Removes all elements from associative array <arrayname>
function AUnset
{
  declare Arr=$1
  eval unset ${Arr}_ind ${Arr}_val ${Arr}_free
}

# Usage: ADelete <arrayname> <index>
# Removes index <index> from associative array <arrayname>
# Returns 0 on success, 1 if <index> was not an index of <arrayname>
function ADelete
{
  declare Arr=$1 Index=$2 End
  NumInd=0
  # Can't use implicit integer referencing on ${Arr}_end here because it may
  # not be set yet.
  eval End=\$${Arr}_end

  Ind ${Arr}_ind "$Index" NumInd $End
  if (( NumInd > 0 )); then
    eval unset ${Arr}_ind[NumInd] ${Arr}_val[NumInd]
    (( NumInd < ${Arr}_free )) && (( ${Arr}_free=NumInd ))
    return 0
  else
    return 1
  fi
}

# Usage: AGetAll <arrayname> <varname>
# All of the indices of array <arrayname> are stored in shell array <varname>
# with indices starting with 0.
# The total number of indices is returned.
function AGetAll
{
  declare -i NElem ElemNum=1 NumNonNull=0
  declare Arr=$1 VarName=$2 ElemVal

  eval NElem=\${#${Arr}_ind[*]}
    while (( ElemNum <= 99999 && NumNonNull < NElem )); do
      eval ElemVal=\"\${${Arr}_ind[ElemNum]}\"
      if [[ -n $ElemVal ]]; then
        eval $VarName[NumNonNull]=\$ElemVal
        ((NumNonNull+=1))
      fi
      ((ElemNum+=1))
    done
  return $NumNonNull
}

# Usage: APrintAll <arrayname> [<sep>]
# For each value stored in <arrayname>, a line containing the index and value
# is printed in the form: index<sep>value
# If <sep> is not passed, '=' is used.
# The total number of indices is returned.
function APrintAll
{
  declare -i NElem ElemNum=1 NumNonNull=0
  declare Arr=$1 Sep=$2 ElemVal ElemInd

  (( $# < 2 )) && Sep="="

  eval NElem=\${#${Arr}_ind[*]}
    while (( ElemNum <= 99999 && NumNonNull < NElem )); do
      eval ElemInd=\"\${${Arr}_ind[ElemNum]}\" \
      ElemVal=\"\${${Arr}_val[ElemNum]}\"
      if [[ -n $ElemInd ]]; then
        echo "$ElemInd$Sep$ElemVal"
        ((NumNonNull+=1))
      fi
      ((ElemNum+=1))
    done
  return $NumNonNull
}

# Usage: ANElem <arrayname>
# The total number of indices in <arrayname> is returned.
function ANElem
{
  eval return \${#${1}_ind[*]}
}

# Read a defaults file
# Usage: ReadDefaults filename var ...
# Any of the named vars that are listed in the file are set globally
function ReadDefaults
{
  declare Defaults var file=$1
  shift

  set_Avars Defaults "$file"
  for var in "$@"; do
    AGet Defaults $var $var
  done
}

# set_Avars: store variable assignments in an associative array.
# 1993-12-28 John H. DuBois III (john@armory.com)
# Converts values to forms that won't be messed with by the shell.
# Usage: set_Avars [-c] array-name [filename ...]
# where the lines in filename (or the input) are of the form
# var=value
# value may contain spaces, backslashes, quote characters, etc.;
# they will become part of the value assigned to index var.
# Lines that begin with a # (optionally preceded by whitespace)
# and lines that do not contain a '=' are ignored.
# Variables are stored in associative array array-name.
# If -c is given, an error message is printed & the program is exited
# if an attempt is made to set a value for a parameter that has already
# been set.

function set_Avars
{
  declare Arr store

  if [[ $1 = -c ]]; then
    store=ChkStore
    shift
  else
    store=AStore
  fi
  Arr=$1
  shift
  for file; do
    if [[ ! -r $file ]]; then
      logMsg "WARNING" "$file: Could not open."
      return 1
    fi
  done
  # return exit status of eval
  eval "$(sed "
/^[ 	]*#/d
  /=/!d
  s/'/'\\\\''/g
  s/=/ '/
  s/$/'/
  s/^/$store $Arr /" "$@")"
}

# Usage: ChkStore <arrname> <index> <value>
# Exit if <index> is already set
function ChkStore
{
  declare arrname=$1 index=$2 value=$3

  if AGet $arrname $index; then
    # 0 return means index not found
    AStore $arrname $index "$value"
  else
    logAbort "$index already set.  Exiting."
  fi
}
#################################################################################################################

function trim
{
  trimmed=$1
  trimmed=$(echo "$trimmed" | sed 's/^[ ]*//;s/[ ]*$//')
  echo "$trimmed"
}  

function checkforldappasswd
{
  NETGROUP=0
  FPASSWDFILE=$PASSWDFILE
  while read line; do
    matched=`echo $line|grep ^+|wc -l`
    if [[ $matched -gt 0 ]]; then
      NETGROUP=1
      return 0 
    fi
  done < $FPASSWDFILE
  logDebug "checkforldappasswd: $NETGROUP"
  return 1
}

function passwd_ids
{
  #passwd_users=""
  
  logDebug "get_passwd_ids: start"
  
  while IFS=: read -r _userid _passwd _uid _gid _gecos _home _shell
  do
    if [[ $_userid = "" ]]; then
      logDebug "get_passwd_ids: Skip empty username"
      continue
    fi
    
    logDebug "get_passwd_ids: add user $_userid"
    
    if [[ $passwd_users = "" ]]; then
      passwd_users="^"$_userid"\$"
    else
      passwd_users=$passwd_users"|^"$_userid"\$";
    fi
  done < /etc/passwd
  IFS=" "
  logDebug "get_passwd_ids: finish $passwd_users"
}  

function Parse_User
{
  ### extracting primary groups from passwd file
  if [[ $PROCESSNIS -eq 1 ]]; then
    FPASSWDFILE=$NISPASSWD
  fi

  if [[ $PROCESSLDAP -eq 1 ]]; then
    FPASSWDFILE=$LDAPPASSWD
  fi	
    
  if [[ $PROCESSNIS -eq 0 && $PROCESSLDAP -eq 0 ]]; then
    FPASSWDFILE=$PASSWDFILE
  fi

  if [[ $IS_ADMIN_ENT_ACC -eq 1 && $NIS -eq 0 && $LDAP -eq 0 && $NOAUTOLDAP -eq 0 ]]; then
    if [[ $OS = "Linux" || $OS = "SunOS" ]]; then
      `getent passwd > $ADMENTPASSWD`
      FPASSWDFILE=$ADMENTPASSWD
      passwd_ids
    fi
  fi

  logDebug "Reading PASSWDFILE: $FPASSWDFILE"

  while IFS=: read -r userid passwd uid gid gecos home shell
    do
      logDebug "Parse_User read userid=$userid passwd=$passwd uid=$uid gid=$gid gecos=$gecos home=$home shell=$shell"
    
      if [[ $userid = "" ]]; then
        logDebug "Skip empty username"
        continue
      fi
    
      if [[ $PROCESSNIS -eq 1 && $NISPLUS -eq 1 ]]; then
        `groups $userid >/dev/null 2>&1`
        if [ $? -ne 0 ]; then
          logDebug "Skip NIS+ user"
          continue
        fi
      fi
        
      if [[ $PROCESSNIS -eq 0 && $PROCESSLDAP -eq 0 ]]; then
        matched=`echo $userid|grep ^+|wc -l`
        if [[ $matched -gt 0 ]]; then
          if [[ $LDAP -eq 0 ]]; then
            logInfo "User $userid is excluded from output file use, -L option to lookup LDAP NetGrp IDs"
            continue
          fi
          matched=`echo $userid|grep ^+@|wc -l`
          if [[ $matched -gt 0 ]]; then
            logDebug "Parse_User: netgroup found $userid"
            Parse_LDAP_Netgrp $userid
            continue
          else
            userid=`echo $userid | tr -d '+'`

            AGet PasswdUser "${userid}" testvar
            if [[ $? -eq 0 ]]; then
              logDebug "Parse_User: netuser found $userid"
              Parse_LDAP_Netuser $userid
            else
              logDebug "User $userid Already exist"
            fi
          fi
        fi
      fi
    AStore PasswdUser ${userid} "$userid"
    AGet primaryGroupUsers ${gid} testvar
    if  [[ $? -eq 0 ]]; then
      AStore primaryGroupUsers ${gid} "$userid"
    else
      AStore primaryGroupUsers ${gid} ",$userid" append
    fi
  done < $FPASSWDFILE
  `rm -f $ADMENTPASSWD`
}

function Parse_Grp
{
  if [[ $PROCESSNIS -eq 1 ]]; then
    FGROUPFILE=$NISGROUP
  fi

  if [[ $PROCESSLDAP -eq 1 ]]; then
    FGROUPFILE=$LDAPGROUP
  fi	
        
  if [[ $PROCESSNIS -eq 0 && $PROCESSLDAP -eq 0 ]]; then
    FGROUPFILE=$GROUPFILE
  fi

  if [[ $IS_ADMIN_ENT_ACC -eq 1 && $NIS -eq 0 && $LDAP -eq 0  && $NOAUTOLDAP -eq 0 ]]; then
    if [[ $OS = "Linux" || $OS = "SunOS" ]]; then
      `getent group > $ADMENTGROUP`
      FGROUPFILE=$ADMENTGROUP
    fi
  fi

  logDebug "Reading GROUPFILE: $FGROUPFILE"

  while IFS=: read -r group gpasswd gid members
    do
      if [[ $group = "" ]]; then
        logDebug "Skip empty groupname"
        continue
      fi
      
      logDebug "Parse_Grp read group=$group gpasswd=$gpasswd gid=$gid members=$members"
      AStore groupGIDName ${gid} "$group"
      
      allusers=""
      AGet primaryGroupUsers ${gid} allusers

      logDebug "Reading in users with $group as a primary group"
      logDebug "grpgid: $gid"
      logDebug "$group pgusers: $allusers"

      if [[ $allusers != "" ]]; then
        if [[ $members != "" ]]; then
          allusers=$allusers",$members"
        else
          allusers=$allusers
        fi
      else
        allusers="$members"
      fi

      logDebug "Reading in $group memberlist from group file"
      logDebug "$group allusers: $allusers"
      logDebug "Uniquifying list"

      AUnset UniqueUsers
      uniqueusers=""
      IFS=,;for nextuser in ${allusers}
        do
          AGet UniqueUsers $nextuser testvar
          if  [[ $? -eq 0 ]]; then
            AStore UniqueUsers  $nextuser "$nextuser"
            if [[ $uniqueusers != "" ]]; then
              uniqueusers=$uniqueusers",$nextuser"
            else
              uniqueusers="$nextuser"
            fi
          else
            continue
          fi
      done
  IFS=" "
        logDebug "Uniqufied allusers:$group $uniqueusers"
  ## storing users ist whihc includes primary groups
        
  testvar=""
  AGet ALLGroupUsers ${group} testvar
  if [[ $testvar != "" ]]; then
    testvar="$testvar,$uniqueusers"
  else
    testvar="$uniqueusers"
  fi
  uniqueusers=$testvar
  AStore ALLGroupUsers "${group}" "$uniqueusers"

  IFS=,;for nextuser in ${uniqueusers}
    do
      AGet AllUserGroups ${nextuser} testvar
      if  [[ $? -eq 0 ]]; then
        AStore AllUserGroups ${nextuser} "$group"
      else
        is_dublicate_checker=0
        for bufgroup in ${testvar}
          do
            if [[ $bufgroup = $group ]]; then
              is_dublicate_checker=1
              break
            fi
          done
        if [[ $is_dublicate_checker -eq 0 ]]; then
          AStore AllUserGroups ${nextuser} ",$group" append
        fi
      fi
    done
  IFS=" "
  matched=0
  if [[ $OS = "Linux" ]]; then
    logDebug "Found Linux"
      matched=`echo $group|egrep $PRIVGROUPS|wc -l`   #V4.5 PRIV group in linux
    if [[ $gid -lt 100 ]]; then
      logDebug "Found privileged group $group $gid < 100"
      logDebug "Adding group: $group:lt100"
      matched=1
    fi
  else
    matched=`echo $group|egrep $PRIVGROUPS|wc -l`
  fi

  if [[ $matched -gt 0 ]]; then
    logDebug "Found Priv group: $group:""$members"
    IFS=,;for nextuser in ${uniqueusers}
    do
      AGet privUserGroups ${nextuser} testvar
      if  [[ $? -eq 0 ]]; then
        AStore privUserGroups ${nextuser} "$group"
      else
        is_dublicate_checker=0
        for bufgroup in ${testvar}
        do
          if [[ $bufgroup = $group ]]; then
            is_dublicate_checker=1
            break
          fi
        done

        if [[ $is_dublicate_checker -eq 0 ]]; then
          AStore privUserGroups ${nextuser} ",$group" append
        fi
      fi
    done
    IFS=" "
  fi
  done < $FGROUPFILE
  `rm -f $ADMENTGROUP`
}

function parse_LDAP_grp
{
  logDebug "parse_LDAP_grp: reading LDAP group"
  if [[ $LDAPFILE = "" ]]; then
    DATA=`$LDAPCMD -LLL -h $LDAPSVR -p $LDAPPORT -b $LDAPBASE objectClass=posixGroup cn gidNumber memberUid >> $ldap_tmp`
  else
    DATA=`$LDAPCMD -LLL -h $LDAPSVR -p $LDAPPORT -b $LDAPBASEGROUP $LDAPADDITIONAL objectClass=$LDAPGROUPOBJCLASS cn gidNumber memberUid >> $ldap_tmp`
  fi  
  awk "  /^cn:/ { print }" $ldap_tmp | cut -d" "  -f2 > $ldap_tmp1
  
  group=""
  gid=""
  gmem=""

  IFS=" "
  while read group; do
    attr=`awk " { RS="\n\n" }  /^dn: cn=$group,/ { print }" $ldap_tmp | sed 's/: /:/g'`
    logDebug "parse_LDAP_grp->read attr=$attr"
    gmem=""
    if echo "$attr" | grep -i "gidNumber:" > /dev/null; then
      gid=$(echo "$attr" | sed -n 's/^gidNumber:\(.*\)/\1/p')
    fi
    if echo "$attr" | grep -i "memberUid:" > /dev/null; then
      gmem=$(echo "$attr" | sed -n 's/^memberUid:\(.*\)/\1/p' | tr ['\n'] [,] )
    fi
    logDebug "parse_LDAP_grp processed group=$group gid=$gid gmem=$gmem"
    echo "$group::$gid:$gmem" >> $LDAPGROUP
    group=""
    gid=""
    gmem=""
  done < $ldap_tmp1
}

function get_state
{
  declare ckid=$1 ostype=$2
  ckid=$(echo $1|sed 's/\//\\\//g')
  state="Enabled"
  
  if [[ $shell = "/bin/false" ]]; then
    state="Disabled"
  fi
  if [[ $shell = "/usr/bin/false" ]]; then
    state="Disabled"
  fi
  
  if [[ $ostype = "AIX" ]]; then
    crypt=`awk "{ RS="\n\n" } /^$ckid:/ { print }" $SPASSWD|grep password|cut -d" " -f3`
    if [[ $crypt = "*" ]]; then
      #logDebug "AIX SPASSWD password * DISABLED $ckid: crypt:$crypt"
      state="Disabled"
    else
      state="Enabled"  
    fi
    
    locked=`awk "{ RS="\n\n" } /^$ckid:/ { print }" $SECUSER|grep account_locked|cut -d" " -f3`
    if [[ $locked = "true" ]]; then
      #logDebug "AIX SECUSER account_locked false DISABLED $ckid: locked:$locked"
      state="Disabled"
    fi
  else
    if [  -r $SPASSWD ]; then
      crypt=`grep ^$ckid: $SPASSWD|cut -d: -f2`
      # check for user disabled by LOCKED, NP, *LK*, !!, or * in password field
      if [[ $crypt = "LOCKED" ]]; then
        state="Disabled"
      fi
      if [[ $crypt = "*" ]]; then
        #logDebug "SPASSWD DISABLED $ckid: crypt:$crypt"
        state="Disabled"
      fi
      if echo "$crypt" | grep "*LK*" > /dev/null; then    #V 4.5
        state="Disabled"
      fi
      if [[ $crypt = "NP" ]]; then
        state="Disabled"
      fi
      if echo "$crypt" | grep "^!" > /dev/null; then
        state="Disabled"
      fi
    fi
  fi
  echo $state
}


# V2.6 iwong
function hpux_get_state
{
    declare ckid=$1 ostype=$2 
    state="Enabled"
    # process shadow file if it exists
    if [  -r $SPASSWD ]; then
      crypt=`grep ^$ckid: $SPASSWD|cut -d: -f2`

      # check for user disabled by LOCKED, NP, *LK*, !!, or * in password field
      if [[ $crypt = "LOCKED" ]]; then
        #logDebug "DEBUG: HPUX SPASSWD DISABLED $ckid: crypt:$crypt" >&2
        state="Disabled"
      fi
      if [[ $crypt = "*" ]]; then
        #logDebug "HPUX SPASSWD DISABLED $ckid: crypt:$crypt" >&2
        state="Disabled"
      fi
      if [[ $crypt = "*LK*" ]]; then
        #logDebug "HPUX SPASSWD DISABLED $ckid: crypt:$crypt" >&2
        state="Disabled"
      fi
    if [[ $crypt = "LK" ]]; then
      #logDebug "HPUX SPASSWD DISABLED $ckid: crypt:$crypt" >&2
      state="Disabled"
    fi
      if [[ $crypt = "NP" ]]; then
        #logDebug "HPUX SPASSWD DISABLED $ckid: crypt:$crypt" >&2
        state="Disabled"
      fi
      if echo "$crypt" | grep "^!" > /dev/null; then          
        #logDebug "HPUX SPASSWD DISABLED $ckid: crypt:$crypt" >&2
        state="Disabled"
      fi
      ## additional check for HP TCB systems
    fi
    # peform getprpw check if TCB machine
    if [[ $TCB_READABLE -eq 1 ]]; then
      lockout=`/usr/lbin/getprpw -m lockout $ckid`
      matched=`echo $lockout|grep 1|wc -l`
      if [[ $matched -gt 0 ]]; then
        #logDebug "HPUX getprpw DISABLED $ckid: $lockout" >&2
        state="Disabled"
      #else
        #logDebug "HPUX getprpw $ckid: $lockout" >&2
      fi
  else
      ISDISABLED=`passwd �s $ckid|egrep "\sLK" | wc -l`
      if [[ $ISDISABLED = "1" ]]; then
        state="Disabled"
      fi  
    fi
    echo $state
}

function ProcessUser_Alias
{
   declare alias=$1 
   logDebug "Starting ProcessUser_Alias:User_Alias: $alias"

   AGet aliasUsers ${alias} aliasusers

   ## process throu list of users
   IFS=,;for nextuser in ${aliasusers}
    do
      ## added code to process groups in the user_alias
      if echo "$nextuser" | grep "^%" >/dev/null; then 
      ## parse out % in group name
        group=`echo ${nextuser}|tr -d %`
        logDebug "ProcessUser_Alias: Found GROUP in User_Alias->$group"

        ## check if goup already read
        AGet sudoGroups $group testvar
        if  [[ $? -eq 0 ]]; then
           ## process through users in the group
          AGet ALLGroupUsers $group uniqueusers
          if [[ $? -eq 0 ]]; then
            logMsg "WARNING" "Invalid group in $SUDOERFILE in User_Alias $alias: $group"
            EXIT_CODE=1
            #let errorCount=errorCount+1
          else
            AStore sudoGroups  ${group} "$alias" 
            logDebug "ProcessUser_Alias: SUDOERS: User_Alias Adding group: $group:$alias"
            IFS=,;for nextu in ${uniqueusers}
            do
              AGet sudoUserGroups ${nextu} testvar
              if  [[ $? -eq 0 ]]; then
                AStore sudoUserGroups ${nextu} "$group" 
              else
                  if echo "$testvar" | grep -i "$group" > /dev/null; then
                    continue
                  else
                   AStore sudoUserGroups ${nextu} ",$group" append
                 fi  
              fi
            done
            IFS=" "
         fi
        else
          logDebug "ProcessUser_Alias: WARNING: User_Alias group: $group read in already"
          continue
        fi
      else
        AGet PasswdUser $nextuser testvar
        if [[ $? -eq 0 ]]; then
           logMsg "WARNING" "Invalid user in $SUDOERFILE in User_Alias $alias: $nextuser"
          EXIT_CODE=1
          #let errorCount=errorCount+1
        else
           userAlias=$nextuser":"$alias
          AGet sudoUsers ${nextuser} testvar
          if  [[ $? -eq 0 ]]; then
            AStore sudoUsers ${nextuser} ${nextuser}
            logDebug "ProcessUser_Alias: User_Alias Adding user to sudoUsers: $userAlias"
          else 
            logDebug "ProcessUser_Alias: WARNING: user already in sudoUsers: $userAlias"
            continue
          fi
        fi
      fi
   done
   IFS=" "
   logDebug "Finished ProcessUser_Alias:User_Alias: $alias"
}

Remove_Labeling_Delimiter()
{
  declare labellingData=$1
  
  outLabellingData=`echo "$labellingData" | sed "s/|/ /g"`
  echo "$outLabellingData"
  return 0
}

GetURTFormat()
{
  declare _gecos=$1

  userstatus="C"
  userccc=$USERCC
  userserial=""
  usercust=""
  usercomment=$_gecos

  ## LOOK FOR CIO Format
  matched=`echo $_gecos | grep -i "s\=" | wc -l`
  if [[ $matched -gt 0 ]]; then
    serialccc=$(echo $gecos | tr "[:upper:]" "[:lower:]" | sed -n 's/.*\(s=[a-zA-Z0-9]*\).*/\1/p')
    serial=$(echo $serialccc|cut -c3-8)
    ccc=$(echo $serialccc|cut -c9-11)

    if [[ ${#serialccc} -ge 11 ]]; then
      userserial=$serial
      userccc=$ccc
      userstatus="I"
      usercust=""
      usercomment=$_gecos
    fi
  fi

    ## LOOK FOR IBM SSSSSS CCC Format
  matched=`echo $_gecos | grep "IBM [a-zA-Z0-9-]\{6\} [a-zA-Z0-9]\{3\}" | wc -l`
    if [[ $matched -gt 0 ]]; then
      oIFS="$IFS"; IFS=' ,' 
      declare -a tokens=($_gecos)
      IFS="$oIFS"

      count=0
      while(( $count < ${#tokens[*]} )); do
      if [[ ${tokens[$count]} = "IBM" ]]; then
        if [[ count+3 -gt ${#tokens[*]} ]]; then
          break
        fi

        serial=${tokens[$count+1]}
        ccc=${tokens[$count+2]}
        if [[ ${#serial} -ne 6 ]]; then
          break
        fi
      if [[ ${#ccc} -ne 3 ]]; then
          break
        else
        ccc3=$(echo $ccc}|cut -c1-3)
      fi

      userserial=$serial
      userccc=$ccc3
      userstatus="I"
      usercomment=$_gecos
      break
    fi
    let count=count+1
    done
    IFS=$oIFS
  fi

  usergecos="$userccc/$userstatus/$userserial/$usercust/$usercomment"

  ## LOOK FOR URT Format
  matched=`echo $_gecos | grep ".\{2,3\}\/.\{1\}\/" | wc -l`
  if [[ $matched -gt 0 ]]; then
    usergecos=$_gecos
  fi
  IFS=" "

  usergecos=`Remove_Labeling_Delimiter "$usergecos"`

  echo "$usergecos"
}

function preparsesudoers
{
  declare sudo_file=$1
  declare tmp_sudo=$2
  declare include_file=""
  declare include_dir=""
  
  logDebug "Preprocess sudo file $sudo_file";
 `cat $sudo_file >> $tmp_sudo`
  while read nextline; do
    if echo "$nextline" | egrep -i "#includedir" > /dev/null; then
      include_dir=`echo "$nextline" | awk '{print $2}'`
      declare content=`ls $include_dir`
      IFS="
      ";for include_file in $content
      do
        if [ ! -e $include_file ]; then
          logDebug "SUDOERS:$include_file is not a file"
          continue
        fi  
        
        if echo "$include_file" | grep -i "~$" > /dev/null; then
          logDebug "SUDOERS: Skip file $include_file"
          continue
        fi
        
        if echo "$include_file" | grep -i "\." > /dev/null; then
          logDebug "SUDOERS: Skip file $include_file"
          continue
        fi
        
        include_file=$include_dir$include_file
        if [ -d $include_file ]; then
          logDebug "SUDOERS:Skip directory $include_file"
          continue
        fi
        logDebug "SUDOERS: Found #includedir directive. $include_dir"
        preparsesudoers $include_file $tmp_sudo
      done
      IFS=" "    
      continue
    fi  
    
    if echo "$nextline" | egrep -i "#include" > /dev/null; then
      include_file=`echo "$nextline" | awk '{print $2}'`
      
      if echo "$include_file" | grep -i "%h$" > /dev/null; then
        include_file=${include_file%%\%h}
        include_file=$include_file"$HOST"
        logDebug "SUDOERS: Add host name to sudo file $include_file"
      fi
      
      if [ ! -e $include_file ]; then
         logDebug "SUDOERS:$include_file is not a file"
         continue
      fi  
      
      logDebug "SUDOERS: Found #include directive. $include_file"
      preparsesudoers $include_file $tmp_sudo
    fi
  done < $sudo_file
}

function Parse_Sudo
{
  declare tmp_sudo_file="/tmp/sudoersfile.tmp"
  `rm -f $tmp_sudo_file`

  preparsesudoers $SUDOERFILE $tmp_sudo_file
  
  SUDOALL="2"
  # egrep removes comments
  # egrep removes netgroup id ( any id starting with +)
  # sed remove leading and trailing spaces
  # sed -e join line with backslash
  # sed replace = with blank
  # sed replace tab with blank
  # tr remove multiple spaces
  # sed delete blank lines
  # remove space between commas
  # remove space between =
  
  DATA=`egrep -v "^#" $tmp_sudo_file| sed 's/^\+\(.*\)/LDAP\/\1/g' | sed 's/^[    ]*//;s/[	 ]*$//'|sed -e :a -e '/\\\\$/N; s/\\\\\n//; ta'|sed 's/	/ /g'|tr -s '[:space:]'|sed '/^$/d'|sed 's/, /,/g'|sed 's/ ,/,/g'|sed 's/ =/=/g'|sed 's/= /=/g'>$TMPFILE` 

  while read nextline; do
    #echo  "SUDOERS: $nextline "
    declare -a tokens=(`echo $nextline`)
    logDebug "SUDOERS: ----> $nextline"
    case ${tokens[0]} in
      Cmnd_Alias ) continue ;;
      Runas_Alias )continue ;;
      Defaults )continue ;;
      ALL )
      if echo "$nextline" | egrep "(!|NOEXEC)" > /dev/null; then
        logMsg "WARNING" "Found ALL=!Cmnd_Alias $nextline"
        SUDOALL="0"
      else
        if [[ $SUDOALL = "2" ]]; then
        tmphostalias=${tokens[1]}
        tmphostalias=${tmphostalias%%=*}
        tmphostalias=`trim "$tmphostalias"`
        
        AGet validHostAlias $tmphostalias testvar
        if [[ $? -ne 0 || $tmphostalias = "ALL" ]]; then
          logDebug "Found SUDOALL $tmphostalias"
          SUDOALL="1"
        fi        
      fi
    fi
    continue
    ;;
    Host_Alias )
    declare -a HAtokens=(`echo $nextline|sed 's/=/ /g'`)
    alias=${HAtokens[1]}
    aliashosts=${HAtokens[2]}
    ## add alias name in to array
    logDebug "SUDOERS HOST ALIAS: $nextline"
    logDebug "SUDOERS HOST ALIAS: Found host alias: $alias"
    ## process throu list of hosts
    IFS=,;for nexthost in ${aliashosts}
      do
        logDebug "SUDOER HOST ALIAS: Host_Alias: $alias checking $nexthost = $HOST"
        ## added code to process groups in the user_alias
        if [[ $nexthost = $HOST || $nexthost = $LONG_HOST_NAME ]]; then
          AStore validHostAlias ${alias} $alias
          logDebug "SUDOER HOST ALIAS: Found valid Host_Alias $alias = $HOST"
          continue
        fi
      done
      IFS=" "
      ;;
      User_Alias )
      declare -a UAtokens=(`echo $nextline|sed 's/=/ /g'`)
      alias=${UAtokens[1]}
      aliasusers=${UAtokens[2]}
      ## add alias name in to array
      logDebug "SUDOERS USER ALIAS: $nextline"
      logDebug "SUDOERS USER ALIAS: Found user alias: $alias"
      AStore aliasUsers ${alias} $aliasusers
      
      IFS=,;for usr in $aliasusers
      do
        AGet UserAliasList $usr testvar
        if  [[ $? -ne 0 ]]; then
          testvar=$testvar",$aliasusers"
          AStore UserAliasList ${usr} $testvar
        else
          AStore UserAliasList ${usr} $alias
        fi    
       done   
       IFS=" "
      ;;
      * )
      #Checking to see if this is a valid host/Host_Alias/ALL
      logDebug "SUDOERS USER/GROUP: $nextline"
      PROCESS_LINE="0"

      for nexttoken in ${nextline}
        do
          logDebug "SUDOERS USER/GROUP: checking nexttoken: $nexttoken"
          if echo "$nexttoken" | grep "=" >/dev/null; then
            hosttoken=`echo $nexttoken|cut -d"=" -f1`
            logDebug "SUDOERS USER/GROUP: FOUND nexttoken: $nexttoken"
            logDebug "SUDOERS USER/GROUP: FOUND hosttoken: $hosttoken"

            # process throu each hostname and Host_alias
            IFS=,;for nexthost in ${hosttoken}
              do
                logDebug "SUDOERS USER/GROUP: FOUND nexthost: $nexthost"
                ## process through users in the group
                ## Check to seeing if valid host_alias
                AGet validHostAlias $nexthost testvar
                if [[ $? -eq 0 ]]; then
                  logDebug "INFO: Not a valid Host_alias: $nexthost"
                else
                  logDebug "INFO: Found a valid Host_alias: $nexthost"
                  PROCESS_LINE="1"
                fi

              if [[ $nexthost = "ALL" ]]; then
                logDebug "INFO: Found ALL: $nexthost"
                PROCESS_LINE="1"
                elif [[ $nexthost = $HOST || $nexthost = $LONG_HOST_NAME ]]; then
                  logDebug "INFO: Found match hostname $HOST: $nexthost"
                  PROCESS_LINE="1"
                fi
                ## Check to seeing if valid hostname
              done
              IFS=" "
            fi
          done

          logDebug "SUDOERS: PROCESS_LINE ->$PROCESS_LINE"
          if [[ $PROCESS_LINE -eq 1 ]]; then
            tokenlist=${tokens[0]}

            if echo "$tokenlist" | grep "^\@" > /dev/null; then   # V4.5
              tokenlist=`echo ${tokenlist}|tr -d @`
              echo "$tokenlist is netgrp"
              AGet Netgrplist $tokenlist testvar
              if [[ $? -ne 0 ]]; then
                tokenlist=$testvar
                logDebug "SUDOERS USER: Adding netgrp list $tokenlist"
              fi
            fi

            IFS=,;for nexttoken in ${tokenlist}
              do

                ## process token if group
                if echo "$nexttoken" | grep "^%" >/dev/null; then
                  ## parse out % in group name
                  group=`echo ${nexttoken}|tr -d %`
                  logDebug "SUDOERS GROUP: Found GROUP ->$group"

                  AGet sudoGroups $group testvar
                  ## check if goup already read
                  if  [[ $? -eq 0 ]]; then
                    ## process through users in the group
                    AGet ALLGroupUsers $group uniqueusers
                    if [[ $? -eq 0 ]]; then
                      logMsg "WARNING" "Invalid group in $SUDOERFILE: $group"
                      #let errorCount=errorCount+1
                      EXIT_CODE=1
                    else
                    logDebug "SUDOERS GROUP: Adding group: $group:group"
                    AStore sudoGroups  ${group} "group"
                    IFS=,;for nextu in ${uniqueusers}
                      do
                        #echo "#------- values from ${nextuser}"
                        AGet sudoUserGroups ${nextu} testvar
                        if  [[ $? -eq 0 ]]; then
                          AStore sudoUserGroups ${nextu} "$group"
                          #print "AStore add to sudoUserGroups RC=$?"
                        else
                          if echo "$testvar" | grep -i "$group" > /dev/null; then
                            continue
                          else  
                            AStore sudoUserGroups ${nextu} ",$group" append
                          fi  
                                
                          #AStore sudoUserGroups ${nextu} ",$group" append
                        #print "AStore add to sudoUserGroups RC=$?"
                      fi
                    done
                    IFS=" "
                  fi
                else
                logDebug "SUDODERS GROUP: WARNING: group: $group read in already"
                continue
              fi
              ## else process as user
            else
            nextuser=$nexttoken
            logDebug "SUDOERS USER: nextuser ->$nextuser"

            ## cheack if this is an user_alias
            AGet aliasUsers $nextuser testvar
            if  [[ $? -ne 0 ]]; then
              logDebug "SUDOERS USER:  Matched User_Alias->$nextuser"
              ProcessUser_Alias $nextuser
              continue
            fi
            AGet PasswdUser $nextuser testvar
            if [[ $? -eq 0 ]]; then
              logMsg "WARNING" "Invalid user in $SUDOERFILE: $nextuser"
              #let errorCount=errorCount+1
              EXIT_CODE=1
            else
            AGet sudoUsers $nextuser testvar
            if  [[ $? -eq 0 ]]; then
              AStore sudoUsers ${nextuser} "sudo"
              logDebug "SUDOERS USER: Adding user: $nextuser"
            else
            logDebug "SUDOERS USER: WARNING: user: $nextuser read in already"
            continue
          fi
        fi
      fi
    done
    IFS=" "
  fi
  ;;
  esac
  done < $TMPFILE
  if [[ $SUDOALL = "2" ]]; then
    SUDOALL="0"
  fi
  
  `rm -f $tmp_sudo_file`
}

Get_Last_Logon_User_Id() {
  declare userID=$1

  LAST_LODIN_DATE=""

  if [[ $OS = 'Linux' ]]; then
    LOGIN_DATA=`lastlog -u $userID 2>/dev/null | grep "$userID" | grep -v grep`

    NEVER_LOGGED_IN=`echo "$LOGIN_DATA" | awk '{if($0 ~ /Never logged in/){print $0}}'`

    if [[ $LOGIN_DATA != "" && $NEVER_LOGGED_IN = "" ]]; then
    wordcount=`echo "$LOGIN_DATA" | wc -w`
    wordcount=`trim $wordcount`
    if [[ "9" = $wordcount ]]; then    
      LAST_LOGIN_YEAR=`echo "$LOGIN_DATA" | awk '{print $9}' | tr -d '\n'`
      LAST_LOGIN_MONTH=`echo "$LOGIN_DATA" | awk '{print $5}' | tr -d '\n'`
      LAST_LOGIN_DAY=`echo "$LOGIN_DATA" | awk '{print $6}' | tr -d '\n'`
      LAST_LOGIN_TIME=`echo "$LOGIN_DATA" | awk '{print $7}' | tr -d '\n'`
    else
      LAST_LOGIN_YEAR=`echo "$LOGIN_DATA" | awk '{print $8}' | tr -d '\n'`
      LAST_LOGIN_MONTH=`echo "$LOGIN_DATA" | awk '{print $4}' | tr -d '\n'`
      LAST_LOGIN_DAY=`echo "$LOGIN_DATA" | awk '{print $5}' | tr -d '\n'`
      LAST_LOGIN_TIME=`echo "$LOGIN_DATA" | awk '{print $6}' | tr -d '\n'`
    fi  
      LAST_LODIN_DATE=$LAST_LOGIN_DAY" "$LAST_LOGIN_MONTH" "$LAST_LOGIN_YEAR
    fi
    elif [[ $OS = 'AIX' ]]; then
      LOGIN_DATA=`lsuser -f $userID 2>/dev/null | grep time_last_login | grep -v grep | sed -e "s/.*=//"`
      if [[ $LOGIN_DATA != "" ]]; then
        if [ -e $PERL ]; then
          LOGIN_DATA=`$PERL -e "print scalar(localtime($LOGIN_DATA))"`
        fi
      fi

      if [[ $LOGIN_DATA != "" ]]; then
        LAST_LOGIN_YEAR=`echo "$LOGIN_DATA" | awk '{print $5}' | tr -d '\n'`
        LAST_LOGIN_MONTH=`echo "$LOGIN_DATA" | awk '{print $2}' | tr -d '\n'`
        LAST_LOGIN_DAY=`echo "$LOGIN_DATA" | awk '{print $3}' | tr -d '\n'`
        LAST_LOGIN_TIME=`echo "$LOGIN_DATA" | awk '{print $4}' | tr -d '\n'`

        LAST_LODIN_DATE=$LAST_LOGIN_DAY" "$LAST_LOGIN_MONTH" "$LAST_LOGIN_YEAR
      fi
    else
        CURRENT_YEAR=`date +%Y`
        CURRENT_MONTH=`date +%b`

      ON_SINCE_DATA=`finger $userID 2>/dev/null | awk '{if($0 ~ /On since/){ printf( "%s,", $0 ) }}'`

      if [[ $ON_SINCE_DATA != "" ]]; then
      # Work with situation when user still works with an account 
	    ON_SINCE_DATA=`echo "$ON_SINCE_DATA" | sed -e "s/.*On since //" | sed -e "s/ on.*//"`			

        PROCESSING_DATA=`echo "$ON_SINCE_DATA" | awk '{ if ($0 ~ /,/) {print $0}}'`

        if [[ $PROCESSING_DATA != "" ]]; then
          # Found the last login year
          LAST_LOGIN_YEAR=`echo "$ON_SINCE_DATA" | awk '{print $4}' | tr -d '\n'`
          LAST_LOGIN_MONTH=`echo "$ON_SINCE_DATA" | awk '{print $2}' | tr -d '\n'`

                lastLoginMonth=""
                curLoginMonth=""

                AGet MNames "$LAST_LOGIN_MONTH"                
                if  [[ $? -ne 0 ]]; then
                    AGet MNames "$LAST_LOGIN_MONTH" lastLoginMonth
                fi

                AGet MNames "$CURRENT_MONTH"                
                if  [[ $? -ne 0 ]]; then
                    AGet MNames "$CURRENT_MONTH" curLoginMonth
                fi

                if [[ $lastLoginMonth != "" && $curLoginMonth != "" ]]; then
                    if [[ $lastLoginMonth -lt 7 && $curLoginMonth -gt 6 ]]; then
                        ((LAST_LOGIN_YEAR -= 1))
                    fi
                fi

          LAST_LOGIN_DAY=`echo "$ON_SINCE_DATA" | awk '{ if($3 ~ /,/){outString=substr($3, 0, length($3)-1);print outString;}else{print $3}}' | tr -d '\n'`
          LAST_LOGIN_TIME=""
	    else
                LAST_LOGIN_YEAR=`date +%Y`
                LAST_LOGIN_MONTH=`echo "$ON_SINCE_DATA" | awk '{print $1}' | tr -d '\n'`
                LAST_LOGIN_DAY=`echo "$ON_SINCE_DATA" | awk '{print $2}' | tr -d '\n'`
                LAST_LOGIN_TIME=`echo "$ON_SINCE_DATA" | awk '{print $3}' | tr -d '\n'`
        fi

        LAST_LODIN_DATE=$LAST_LOGIN_DAY" "$LAST_LOGIN_MONTH" "$LAST_LOGIN_YEAR
      fi

      LAST_LOGIN=`finger $userID 2>/dev/null | awk '{if($0 ~ /Last login/){ print $0 }}'`

      if [[ $LAST_LOGIN != "" ]]; then
        LAST_LOGIN=`echo "$LAST_LOGIN" | sed -e "s/Last login //" | sed -e "s/ on.*//"`

        PROCESSING_DATA=`echo "$LAST_LOGIN" | awk '{ if ($0 ~ /,/) {print $0}}'`

        if [[ $PROCESSING_DATA != "" ]]; then
          # Found the last login year
          LAST_LOGIN_YEAR=`echo "$LAST_LOGIN" | awk '{print $4}' | tr -d '\n'`
          LAST_LOGIN_MONTH=`echo "$LAST_LOGIN" | awk '{print $2}' | tr -d '\n'`

          LAST_LOGIN_DAY=`echo "$LAST_LOGIN" | awk '{ if($3 ~ /,/){outString=substr($3, 0, length($3)-1);print outString;}else{print $3}}' | tr -d '\n'`
          LAST_LOGIN_TIME=""
	    else
		if [ $OS = 'SUNOS' ]; then
		    LAST_LOGIN_YEAR=`date +%Y`
		    LAST_LOGIN_MONTH=`echo "$LAST_LOGIN" | awk '{print $2}' | tr -d '\n'`

                    lastLoginMonth=""
                    curLoginMonth=""

                    AGet MNames "$LAST_LOGIN_MONTH"                
                    if  [[ $? -ne 0 ]]; then
                        AGet MNames "$LAST_LOGIN_MONTH" lastLoginMonth
                    fi

                    AGet MNames "$CURRENT_MONTH"                
                    if  [[ $? -ne 0 ]]; then
                        AGet MNames "$CURRENT_MONTH" curLoginMonth
                    fi

                    if [[ $lastLoginMonth != "" && $curLoginMonth != "" ]]; then
                        if [[ $lastLoginMonth -lt 7 && $curLoginMonth -gt 6 ]]; then
                            ((LAST_LOGIN_YEAR -= 1))
                        fi
                    fi

        LAST_LOGIN_DAY=`echo "$LAST_LOGIN" | awk '{print $3}' | tr -d '\n'`
        LAST_LOGIN_TIME=`echo "$LAST_LOGIN" | awk '{print $4}' | tr -d '\n'`
    else
        LAST_LOGIN_YEAR=`date +%Y`
        LAST_LOGIN_MONTH=`echo "$LAST_LOGIN" | awk '{print $1}' | tr -d '\n'`

                    lastLoginMonth=""
                    curLoginMonth=""

                    AGet MNames "$LAST_LOGIN_MONTH"                
                    if  [[ $? -ne 0 ]]; then
                        AGet MNames "$LAST_LOGIN_MONTH" lastLoginMonth
                    fi

                    AGet MNames "$CURRENT_MONTH"                
                    if  [[ $? -ne 0 ]]; then
                        AGet MNames "$CURRENT_MONTH" curLoginMonth
                    fi

                    if [[ $lastLoginMonth != "" && $curLoginMonth != "" ]]; then
                        if [[ $lastLoginMonth -lt 7 && $curLoginMonth -gt 6 ]]; then
                            ((LAST_LOGIN_YEAR -= 1))
                        fi
          fi

        LAST_LOGIN_DAY=`echo "$LAST_LOGIN" | awk '{print $2}' | tr -d '\n'`
        LAST_LOGIN_TIME=`echo "$LAST_LOGIN" | awk '{print $3}' | tr -d '\n'`
    fi
      fi

        LAST_LODIN_DATE=$LAST_LOGIN_DAY" "$LAST_LOGIN_MONTH" "$LAST_LOGIN_YEAR
      fi
    fi

  echo $LAST_LODIN_DATE
}

function report
{
  AUTHKEYSFILE=""
  AUTHKEYSFILE2=""
  PUBKEYAUTH=""
  #V4.4 Code to check SSH public key authentation status for users having password "*" in passwd file
  if [[ $OS = "SunOS" ]]; then
    SSHD_CONFIG="/etc/ssh/sshd_config"
    if [ -e "/usr/local/etc/sshd_config" ]; then
      SSHD_CONFIG="/usr/local/etc/sshd_config"
    fi
    AUTHKEYSFILE=`grep 'AuthorizedKeysFile[[:space:]]\{1,\}' $SSHD_CONFIG | grep -v "\#" | nawk {'print $2'}`
    AUTHKEYSFILE2=`grep 'AuthorizedKeysFile2' $SSHD_CONFIG | grep -v "\#" | nawk {'print $2'}`
    PUBKEYAUTH=`grep PubkeyAuthentication $SSHD_CONFIG | grep -v "\#" | nawk {'print $2'}`
  else
    if [ -e "/etc/ssh/sshd_config" ]; then
      AUTHKEYSFILE=`grep 'AuthorizedKeysFile[[:space:]]\{1,\}' /etc/ssh/sshd_config | grep -v "\#" | awk {'print $2'}`
      AUTHKEYSFILE2=`grep 'AuthorizedKeysFile2' /etc/ssh/sshd_config | grep -v "\#" | awk {'print $2'}`
      PUBKEYAUTH=`grep PubkeyAuthentication /etc/ssh/sshd_config | grep -v "\#" | awk {'print $2'}`
    fi
  fi

  if [[ $AUTHKEYSFILE = "" ]]; then
    AUTHKEYSFILE=".ssh/authorized_keys"
  fi
  
  if [[ $AUTHKEYSFILE2 = "" ]]; then
    AUTHKEYSFILE2=".ssh/authorized_keys2"
  fi
  
  logDebug "Authorized_keys file path:$AUTHKEYSFILE and SSH public key auth enabled is $PUBKEYAUTH "

  if [[ $PROCESSNIS -eq 1 ]]; then
    FPASSWDFILE=$NISPASSWD
  fi

  if [[ $PROCESSLDAP -eq 1 ]]; then
    FPASSWDFILE=$LDAPPASSWD
	fi

  if [[ $PROCESSNIS -eq 0 && $PROCESSLDAP -eq 0 ]]; then
    FPASSWDFILE=$PASSWDFILE
  fi

  if [[ $IS_ADMIN_ENT_ACC -eq 1 && $NIS -eq 0 && $LDAP -eq 0  && $NOAUTOLDAP -eq 0 ]]; then
    if [[ $OS = "Linux" ]]; then
      `getent passwd > $ADMENTPASSWD`
      FPASSWDFILE=$ADMENTPASSWD
      elif [[ $OS = "SunOS" ]]; then
        `getent passwd > $ADMENTPASSWD`
        FPASSWDFILE=$ADMENTPASSWD
      fi
    fi

  DATA=`cat $FPASSWDFILE > $TMPFILE`
  if [[ $LDAP -eq 1 && $NETGROUP -eq 1 && -f $LDAPPASSWD ]]; then
    DATA=`cat $LDAPPASSWD >> $TMPFILE`
  fi

  while IFS=: read -r userid passwd uid gid gecos home shell
    do
      logDebug "report->read userid=$userid passwd=$passwd uid=$uid gid=$gid gecos=$gecos home=$home shell=$shell"
      matched=`echo $userid|grep ^+|wc -l`
      if [[ $matched -gt 0 ]]; then
        continue
      fi
      
      if [[ $userid = "" ]]; then
        continue
      fi
              
      gecos=`Remove_Labeling_Delimiter "$gecos"`
      
      privilege=""
      pgroup=""
      userstate="Enabled"
      userllogon=""
      privField=""
      groupField=""
      privGroup=""
      userllogon=""
      
      if [[ $DLLD -eq 0 ]]; then
        userllogon=`Get_Last_Logon_User_Id "$userid"`
      fi
      
      AGet groupGIDName $gid testvar
            
      matched=`echo $userid|egrep $PRIVUSERS|wc -l`
      if [[ $matched -gt 0 ]]; then
          logDebug "Found privileged ID:$userid"
          privField="$userid"
      fi

      if [[ $OS = "Linux" && $gid -lt 100 ]]; then
        if [[ matched -eq 0 ]]; then
          logDebug "Found privileged ID: $userid"
          if [[ $privField != "" ]]; then
            privField="$privField,$userid"
          else
            privField="$userid"
          fi
        fi
      fi

      AGet privUserGroups $userid testvar
      if  [[ $? -ne 0 ]]; then
        privGroup="GRP($testvar)"
        if [[ $privField != "" ]]; then
          privField=$privField",$privGroup"
        else
          privField=$privGroup
        fi
      fi

      if [[ $SUDOALL = "1" ]]; then
        if [[ $privField != "" ]]; then
          privField=$privField",SUDO_ALL"
        else
          privField="SUDO_ALL"
        fi
      fi
      
      AGet sudoUserGroups $userid testvar
      if  [[ $? -ne 0 ]]; then
        sudoGroup="SUDO_GRP($testvar)"
        if [[ $privField != "" ]]; then
          privField=$privField",$sudoGroup"
        else
          privField=$sudoGroup
        fi
      fi

      AGet UserAliasList $userid testvar
      if  [[ $? -ne 0 ]]; then
        if [[ $privField != "" ]]; then
          privField="$privField,SUDO_ALIAS($testvar)"
        else
          privField="SUDO_ALIAS($testvar)"
        fi
        ADelete UserAliasList $userid
        logDebug "SUDOERS: deleting UserAliasList arrays entry: $userid"
      fi

      AGet sudoUsers $userid testvar
      if  [[ $? -ne 0 ]]; then
        if [[ $privField != "" ]]; then
          privField="$privField,SUDO_$userid"
        else
          privField="SUDO_$userid"
        fi
        ADelete sudoUsers $userid
        logDebug "SUDOERS: deleting sudoUser array entry: $userid"
      fi

      AGet AllUserGroups $userid groupField
      if [[ $? -eq 0 ]]; then
        logMsg "WARNING" "user $userid is in group $gid. Unable to resolve group $gid to a name"
        EXIT_CODE=1
      fi  

      if [[ $OS = "Tru64" || $OS = "OSF1" ]]; then
        userstate=`hpux_get_state $userid $OS`
        logDebug "Report: user $userid state $userstate"
      fi  
      
      if [[ $OS = "HP-UX" ]]; then
        userstate=`hpux_get_state $userid $OS`
        if [[ $SEC_READABLE -eq 0 && $passwd = "*" ]]; then
          userstate="Disabled"
        fi        
        #echo "____> hpux_get_state:$userstate"
      else
        if [[ $SEC_READABLE -eq 1 ]]; then
          userstate=`get_state $userid $OS`
          logDebug "Report: SEC_READABLE user $userid state $userstate"
          #echo "____> get_state:$userstate"
        else
          #userstate="0"
          userstate="Enabled"
        fi
      fi

      # V2.6 iwong
      if [[ $TCB_READABLE -eq 0 ]]; then
        if [[ $passwd = "*" ]]; then
          if [[ $PUBKEYAUTH = "yes" ]]; then          #v4.4 Code to check SSH public key authentation status for users having password "*" in passwd file
            logDebug "Checking SSH public key file $home/$AUTHKEYSFILE for user $userid"
            if [[ -s $home/$AUTHKEYSFILE || -s $home/$AUTHKEYSFILE2 ]]; then
              userstate="SSH-Enabled"
              logDebug "SSH Key file:$home/$AUTHKEYSFILE is found for $userid"
            else
              #echo "User is disabled"
              userstate="Disabled"
            fi
          else
             if [[ ! ( $OS = "Tru64" || $OS = "OSF1" || $PROCESSNIS ) ]]; then
             userstate="Disabled"
             logDebug "User disabled $userid: passwd:$passwd"
          fi
        fi
      fi
      else
        logDebug "Bypassing * passwd check: $userid"
      fi
      scmstate=""
      if [[ $userstate = "Enabled" ]]; then
        scmstate="0"
      fi
      if [[ $userstate = "Disabled" ]]; then
        scmstate="1"
      fi

      if [[ $PROCESSNIS -eq 1 ]]; then
        userid="NIS/"$userid
      fi

      if [[ $PROCESSLDAP -eq 1 ]]; then
        userid="LDAP/"$userid
      fi

      if [[ $IS_ADMIN_ENT_ACC -eq 1 && $NIS -eq 0 && $LDAP -eq 0 && $NOAUTOLDAP -eq 0 ]]; then
        if [[ $OS = "Linux" || $OS = "SunOS" ]]; then
          matched=`echo $userid|egrep $passwd_users|wc -l`
          if [[ $matched -eq 0 ]]; then
            userid="LDAP/"$userid
          fi
        fi
      fi

      #echo "userstate=$userstate"
      #SCM9 hostname<tab>os<tab>auditdate<tab>account<tab>userIDconv<tab>state<tab>l_logon<tab>group<tab>privilege

      if [[ $SCMFORMAT -eq 1 ]]; then
        echo "$HOSTNAME\t$OS\t$myAUDITDATE\t$userid\t$gecos\t$scmstate\t$userllogon\t$groupField\t$privField" >> $OUTPUTFILE
        elif [[ $MEF2FORMAT -eq 1 ]]; then
          #MEF2 customer|system|account|userID convention data|group|state|l_logon|privilege
          echo "$CUSTOMER|$HOSTNAME|$userid|$gecos|$groupField|$userstate|$userllogon|$privField" >> $OUTPUTFILE
        else
          #MEF3 �customer|identifier type|server identifier/application identifier|OS name/Application name|account|UICMode|userID convention data|state|l_logon |group|privilege�
          echo "$CUSTOMER|S|$HOSTNAME|$OS|$userid||$gecos|$userstate|$userllogon|$groupField|$privField" >> $OUTPUTFILE
      fi

        #hostname<tab>os<tab>auditdate<tab>account<tab>userIDconv<tab>state<tab>l_logon<tab>group<tab>privilege
          #print "$HOST\t$OS\t$myAUDITDATE\t$userid\t$userCC/$userstatus/$userserial/$usercust/$usercomment\t$userstate\t$userllogon\t$pgroup\t$privilege"  >> $OUTPUTFILE

  done < $TMPFILE
  `rm -f $ADMENTPASSWD`
}

function Parse_LDAP_Netuser
{
  ################## Processing LDAP Ids #################
  if [[ $LDAP -eq 1 ]]; then
    gecos=""
    
    IFS=" "
    if [[ $LDAPFILE = "" ]]; then
      attr=`$LDAPCMD -LLL -h $LDAPSVR -p $LDAPPORT -b $LDAPBASE uid=$1 uid userPassword uidNumber gidNumber loginShell gecos description`
    else
      attr=`$LDAPCMD -LLL -h $LDAPSVR -p $LDAPPORT -b $LDAPBASE $LDAPADDITIONAL uid=$1 uid userPassword uidNumber gidNumber loginShell gecos description`
    fi
    
    if [[ $? -ne 0 ]]; then
      logAbort "unable access LDAP server"
    fi
    userid=$(echo "$attr" | sed -n 's/^uid: \(.*\)/\1/p')
    uid=$(echo "$attr" | sed -n 's/^uidNumber: \(.*\)/\1/p')
    gid=$(echo "$attr" | sed -n 's/^gidNumber: \(.*\)/\1/p')
    passwd=$(echo "$attr" | sed -n 's/^userPassword:: \(.*\)/\1/p')
    shell=$(echo "$attr" | sed -n 's/^loginShell: \(.*\)/\1/p')
    gecos=$(echo "$attr" | sed -n 's/^gecos: \(.*\)/\1/p')
    gecos=$(echo "$attr" | sed -n 's/^description: \(.*\)/\1/p')

    echo "$userid:$passwd:$uid:$gid:$gecos:$shell" >> $LDAPPASSWD

    logDebug "Parse_LDAP_Netuser attr is $attr "
    logDebug "Parse_LDAP_Netuser LDAP ID: $userid:$uid:$gid:$gecos:$shell:$passwd"
  fi
}

function Parse_LDAP_Subnetgrp
{
  declare subnetgrp=$1
  declare subldapgroup=""
  declare subattr=""
  declare sub_temp_file=`mktemp`
  
  
  logDebug "Subnetgroup is $subnetgrp"

  if [[ $LDAPFILE = "" ]]; then
    subattr=`$LDAPCMD -LLL -h $LDAPSVR -p $LDAPPORT -b $LDAPBASE cn=$subnetgrp cn nisNetgroupTriple memberNisNetgroup  > $sub_temp_file`
  else
    subattr=`$LDAPCMD -LLL -h $LDAPSVR -p $LDAPPORT -b $LDAPBASEGROUP $LDAPADDITIONAL cn=$subnetgrp cn nisNetgroupTriple memberNisNetgroup  > $sub_temp_file`
  fi

  logDebug "Parse_LDAP_Subnetgrp attr is $subattr"
  
  IFS=" "
  while read -r tmpline subldapgroup
  do  
    if echo "$tmpline" | grep -i "memberNisNetgroup:" > /dev/null; then
        Parse_LDAP_Subnetgrp $subldapgroup
    fi  
  done < $sub_temp_file  

  echo "" >> $LDAP_NETGOUP_TMP
  subattr=`cat $sub_temp_file >> $LDAP_NETGOUP_TMP`

  if [ -a $sub_temp_file ]; then
    rm $sub_temp_file
  fi
}

function Parse_LDAP_Netgrp
{
  if [[ $LDAP -eq 1 ]]; then
    netgrp=`echo $1 | tr -d '+' | tr -d '@' `
    logDebug "Netgroup is $netgrp "
    netgr_temp_file=`mktemp`
    
    if [[ $LDAPFILE = "" ]]; then
      attr=`$LDAPCMD -LLL -h $LDAPSVR -p $LDAPPORT -b $LDAPBASE cn=$netgrp cn nisNetgroupTriple memberNisNetgroup > $LDAP_NETGOUP_TMP`
    else
      attr=`$LDAPCMD -LLL -h $LDAPSVR -p $LDAPPORT -b $LDAPBASEGROUP $LDAPADDITIONAL cn=$netgrp cn nisNetgroupTriple memberNisNetgroup > $LDAP_NETGOUP_TMP`
    fi

    attr1=`cp $LDAP_NETGOUP_TMP $netgr_temp_file`

    logDebug "Parse_LDAP_Netgrp attr is $attr"

    IFS=" "
    while read -r tmpline ldapgroup
    do  
      if echo "$tmpline" | grep -i "memberNisNetgroup:" > /dev/null; then
          Parse_LDAP_Subnetgrp $ldapgroup
      fi
    done < $netgr_temp_file
    
    rm $netgr_temp_file
    
    if grep -i "nisNetgroupTriple:" $LDAP_NETGOUP_TMP > /dev/null; then
      ldapmem=$(sed -n 's/^nisNetgroupTriple:.*,\(.*\),.*/\1/p' $LDAP_NETGOUP_TMP | tr ['\n'] [,] )
    fi

    logDebug "Parse_LDAP_Netgrp $netgrp : $ldapmem"

    IFS=,;for nextuser in ${ldapmem}
      do
        logDebug "Parse_LDAP_Netgrp $nextuser is processing "

        AGet PasswdUser "${nextuser}" testvar
        if [[ $? -eq 0 ]]; then
          Parse_LDAP_Netuser $nextuser
        else
          logDebug "Parse_LDAP_Netgrp User $nextuser Already exist"
          AGet Netgrplist ${netgrp} testvar
          if  [[ $? -eq 0 ]]; then
            AStore Netgrplist ${netgrp} "$nextuser"
          else
            AStore Netgrplist ${netgrp} ",$nextuser" append
          fi
          continue
        fi

        AGet Netgrplist ${netgrp} testvar
        if  [[ $? -eq 0 ]]; then
          AStore Netgrplist ${netgrp} "$userid"
        else
          AStore Netgrplist ${netgrp} ",$userid" append
        fi

        AStore PasswdUser ${userid} "$userid"
        AGet primaryGroupUsers ${gid} testvar
        if  [[ $? -eq 0 ]]; then
          AStore primaryGroupUsers ${gid} "$userid"
        else
          AStore primaryGroupUsers ${gid} ",$userid" append
        fi
        AGet primaryGroupUsers ${gid} testvar
      done
    IFS=" "
  fi
  
  if [ -a $LDAP_NETGOUP_TMP ]; then
    rm $LDAP_NETGOUP_TMP
  fi

}

function IsAdminEntAccessible
{
  if [[ $OS = "AIX" ]]; then
    ret=`lsuser -R LDAP ALL  2>/dev/null`
    if [[ $? -eq 0 ]]; 
    then
      logInfo "Server $HOST ($uname) is LDAP connected"
      IS_ADMIN_ENT_ACC=1
    else
      logInfo "Server $HOST ($uname) is not LDAP connected"
    fi  
  elif [[ $OS = "Linux" ]]; then
      if [[ x"`getent passwd`" = x ]]; then
      logInfo "Server $HOST ($uname) is not support getent utility"   
    else
      IS_ADMIN_ENT_ACC=1    
    fi
  elif [[ $OS = "SunOS" ]]; then
    if [[ x"`getent passwd`" = x ]]; then
      logInfo "Server $HOST ($uname) is not support getent utility"   
    else
      IS_ADMIN_ENT_ACC=1    
    fi
  else
    logInfo "Operating system: $uname not supported for checks of LDAP!"
  fi
  logDebug "IsAdminEntAccessible IS_ADMIN_ENT_ACC=$IS_ADMIN_ENT_ACC"
  return 0
}

function check_pam
{
  if [ -a $LDAPCONF ]; then
    while read line; do 
      if echo "$line" | grep -i "^pam_check_host_attr" > /dev/null; then
        val=$(echo "$line" | sed -n 's/^pam_check_host_attr \(.*\)/\1/p')
        if [[ $val = "yes" ]]; then
          logDebug "pam_check_host_attr yes"
          return 1;
        fi   
      fi
    done < $LDAPCONF
  fi
  logDebug "pam_check_host_attr no"
  return 0
}

function process_LDAP_users
{
  IsPAM=0
  check_pam
  if  [[ $? -eq 1 ]]; then
    IsPAM=1
  fi

  if [[ $LDAPFILE = "" ]]; then
    DATA=`$LDAPCMD -LLL -h $LDAPSVR -b $LDAPBASE -p $LDAPPORT uid=* uid userpassword uidNumber gidNumber loginShell gecos host description >> /tmp/ldap_users`;
  else
    DATA=`$LDAPCMD -LLL -h $LDAPSVR -b $LDAPBASE -p $LDAPPORT $LDAPADDITIONAL uid=* uid userpassword uidNumber gidNumber loginShell gecos host description >> /tmp/ldap_users`;
  fi
    
  if [[ $? -ne 0 ]]; then
    logAbort "unable access LDAP server"
  fi

  firsttime='true'
  userid=''
  passwd=''
  uid=''
  gid=''
  gecos=''
  shell=''
  checkHost=0
  
  while read line; do
    logDebug "process_LDAP_users->read line=$line"

    if echo "$line" | grep -i "uidNumber:" > /dev/null; then
      uid=$(echo "$line" | sed -n 's/^uidNumber: \(.*\)/\1/p')
    fi

    if echo "$line" | grep -i "gidNumber:" > /dev/null; then
      gid=$(echo "$line" | sed -n 's/^gidNumber: \(.*\)/\1/p')
    fi

    if echo "$line" | grep -i "userPassword::" > /dev/null; then
      passwd=$(echo "$line" | sed -n 's/^userPassword:: \(.*\)/\1/p')
    fi

    if echo "$line" | grep -i "loginShell:" > /dev/null; then
      shell=$(echo "$line" | sed -n 's/^loginShell: \(.*\)/\1/p')
    fi

    if echo "$line" | grep -i "gecos:" > /dev/null; then
      gecos=$(echo "$line" | sed -n 's/^gecos: \(.*\)/\1/p')
    fi

    if echo "$line" | grep -i "description:" > /dev/null; then
      gecos=$(echo "$line" | sed -n 's/^description: \(.*\)/\1/p')
    fi

    if echo "$line" | grep -i "uid:" > /dev/null; then
      userid=$(echo "$line" | sed -n 's/^uid: \(.*\)/\1/p')
    fi

    if echo "$line" | grep -i "host:" > /dev/null; then
      host=$(echo "$line" | sed -n 's/^host: \(.*\)/\1/p')
      if [[ $IsPAM -eq 1 && ($host = $HOST || $host = $LONG_HOST_NAME) ]]; then
        checkHost=1
        logDebug "process_LDAP_users userhost=$host"
      fi  
    fi
    
    if echo "$line" | grep -i "dn: " > /dev/null; then
      if [[ $firsttime = 'true' ]]; then
        firsttime='false'
        continue
      fi
      if [[ $OS = "AIX" ]]; then
        testvar=0
        AGet LDAP_users ${userid} testvar
        if [[ $testvar -eq 1 ]]; then
          echo "$userid:$passwd:$uid:$gid:$gecos::$shell" >> $LDAPPASSWD
        else
          logDebug "process_LDAP_users: skip user $userid"
        fi  
      else  
        if [[ $IsPAM -eq 1 ]]; then
          if [[ $checkHost -eq 1 ]]; then 
        echo "$userid:$passwd:$uid:$gid:$gecos::$shell" >> $LDAPPASSWD
          else
            logDebug "process_LDAP_users: skip user $userid"
          fi  
        else
          echo "$userid:$passwd:$uid:$gid:$gecos::$shell" >> $LDAPPASSWD          
        fi  
          checkHost=0
      fi
        
      logDebug "process_LDAP_users->processed userid=$userid passwd=$passwd uid=$uid gid=$gid gecos=$gecos shell=$shell"

      passwd=""
      uid=""
      gid=""
      gecos=""
      shell=""
    fi
    done < /tmp/ldap_users

  if [ -n $userid ]; then
    if [[ $OS = "AIX" ]]; then
      testvar=0
      AGet LDAP_users ${userid} testvar
      if [[ $testvar -eq 1 ]]; then
        echo "$userid:$passwd:$uid:$gid:$gecos::$shell" >> $LDAPPASSWD
      else
        logDebug "process_LDAP_users: skip user $userid"
      fi  
    else  
      if [[ $IsPAM -eq 1 ]]; then
        if [[ $checkHost -eq 1 ]]; then 
      echo "$userid:$passwd:$uid:$gid:$gecos::$shell" >> $LDAPPASSWD
        else
          logDebug "process_LDAP_users: skip user $userid"
        fi  
      else
        echo "$userid:$passwd:$uid:$gid:$gecos::$shell" >> $LDAPPASSWD          
      fi  
    fi
  fi

  if [ -a /tmp/ldap_users ]; then
    rm /tmp/ldap_users
  fi
}

function findSudoersFile
{
  SUDOERFILE="/dev/null"
  SUDOERFILE1="/etc/sudoers"
  SUDOERFILE2="/opt/sfw/etc/sudoers"
  SUDOERFILE3="/usr/local/etc/sudoers"
  SUDOERFILE4="/opt/sudo/etc/sudoers"
  SUDOERFILE5="/opt/sudo/etc/sudoers/sudoers"
  SUDOERFILE6="/usr/local/etc/sudoers/sudoers"
  SUDOERFILE7="/opt/sudo/sudoers"

  if [ -r $SUDOERFILE1 ]; then
    SUDOERFILE=$SUDOERFILE1
  elif [ -r $SUDOERFILE2 ]; then
    SUDOERFILE=$SUDOERFILE2
  elif [ -r $SUDOERFILE3 ]; then
    SUDOERFILE=$SUDOERFILE3
  elif [ -r $SUDOERFILE4 ]; then
    SUDOERFILE=$SUDOERFILE4
  elif [ -r $SUDOERFILE5 ]; then
    SUDOERFILE=$SUDOERFILE5
  elif [ -r $SUDOERFILE6 ]; then
    SUDOERFILE=$SUDOERFILE6
  elif [ -r $SUDOERFILE7 ]; then
    SUDOERFILE=$SUDOERFILE7
  fi
}

function check_nisplus
{
  if [ -s "/var/nis/NIS_COLD_START" ]; then
   return 1
  fi
  return 0;  
}

ClearFile()
{
    declare FILE=$1
    
    `echo "" > $FILE && rm $FILE` 
    if [[ $? -ne 0 ]]; then
      logMsg "WARNING" "Unable to open $FILE"
    fi
}


function Mef_Users_Post_Process
{
    declare outputFile=$1 ibmOnly=$2 customerOnly=$3
    
    isIbmUser=0
    returnCode=0
    
    if [[ $ibmOnly -eq 1 && $customerOnly -eq 1 ]]; then
        return 1
    fi
    
    if [[ $ibmOnly -eq 0 && $customerOnly -eq 0 ]]; then
        return 1
    fi
    
    baseMefName=`basename "$outputFile"`
    tmpOut="/tmp/${baseMefName}_tmp"
    
    if [[ -f "$outputFile" ]]; then
        # Storing file's data
        `echo "" >> "$outputFile"`
        `cat "$outputFile" > "$tmpOut"`
        `echo "" >> "$tmpOut"`
        
        # Clear the output file
        `ClearFile "$outputFile"`
        
        while read -r nextline; do
            if [[ $nextline != "" ]]; then
                isIbmUser=0
                
                CUSTOMER_MEF3=`echo "$nextline" | awk '
                            {
                                split($0, str, "|");
                                print str[1];
                            }
                        '`
                        
                HOST_MEF3=`echo "$nextline" | awk '
                            {
                                split($0, str, "|");
                                print str[3];
                            }
                        '`
                        
                INSTANCE_MEF3=`echo "$nextline" | awk '
                            {
                                split($0, str, "|");
                                print str[4];
                            }
                        '`
                        
                USER_MEF3=`echo "$nextline" | awk '
                            {
                                split($0, str, "|");
                                print str[5];
                            }
                        '`
                        
                FLAG_MEF3=`echo "$nextline" | awk '
                            {
                                split($0, str, "|");
                                print str[6];
                            }
                        '`
                        
                DESCRIPTION_MEF3=`echo "$nextline" | awk '
                            {
                                split($0, str, "|");
                                print str[7];
                            }
                        '`
                        
                USERSTATE_MEF3=`echo "$nextline" | awk '
                            {
                                split($0, str, "|");
                                print str[8];
                            }
                        '`
                        
                USERLLOGON_MEF3=`echo "$nextline" | awk '
                            {
                                split($0, str, "|");
                                print str[9];
                            }
                        '`
                        
                GROUPS_MEF3=`echo "$nextline" | awk '
                            {
                                split($0, str, "|");
                                print str[10];
                            }
                        '`
                        
                ROLES_MEF3=`echo "$nextline" | awk '
                            {
                                split($0, str, "|");
                                print str[11];
                            }
                        '`
                
                # 1. Checking on the signature record
                matched=`echo "$nextline" | egrep "NOTaRealID" | wc -l`
                if [[ $matched -gt 0 ]]; then
                    `echo "$nextline" >> $outputFile`
                    continue
                fi
                
                # 2. Checking if user has this format <login name>@<location>.ibm.com
                SPECIAL_FLAG=`echo $USER_MEF3 | grep -i '.*@.*\.ibm\.com'`                
                if [[ $SPECIAL_FLAG != "" && $ibmOnly -ne 0 ]]; then
                    `echo "$nextline" >> $outputFile`
                    continue
                fi
                
                if [[ $SPECIAL_FLAG != "" && $customerOnly -ne 0 ]]; then
                    continue
                fi
                
                matched=`echo "$DESCRIPTION_MEF3" | grep ".\{3\}\/[^\/]*\/[^\/]*\/[^\/]*\/.*" | wc -l`
                if [[ $matched -eq 0 ]]; then
                    # description of the current userID doesn't contain URT format information in the description field
                    USERGECOS_MEF3=`GetURTFormat "$DESCRIPTION_MEF3"`
                else
                    USERGECOS_MEF3=$DESCRIPTION_MEF3
                fi
                
                matched=`echo "$USERGECOS_MEF3" | grep ".\{3\}\/[^\/]*\/[^\/]*\/[^\/]*\/.*" | wc -l`
                if [[ $matched -ne 0 ]]; then
                    matched=`echo "$USERGECOS_MEF3" | grep ".\{3\}\/[ISFTEN]\/[^\/]*\/[^\/]*\/.*" | wc -l`
                    if [[ $matched -ne 0 ]]; then
                        isIbmUser=1
                    fi
                else
                    returnCode=3
                fi
                
                if [[ $isIbmUser -eq 1 && $ibmOnly -eq 1 ]]; then
                    `echo "$nextline" >> "$outputFile"`
                    continue
                fi
                
                if [[ $isIbmUser -eq 0 && $customerOnly -eq 1 ]]; then
                    `echo "$nextline" >> "$outputFile"`
                    continue
                fi
            fi
        done < "$tmpOut"
    else
        return 2
    fi
    
    `ClearFile "$tmpOut"`
    logInfo "Finished .MEF3 report filtering"  
    return $returnCode
}

function Filter_mef3
 {
  logInfo "Started .MEF3 report filtering"
  logDebug "filter: OutputFile:$OUTPUTFILE"
  logDebug "filter: ibmOnly:$IBMONLY"
  logDebug "filter: customerOnly:$CUSTOMERONLY"

  if [[ $ibmonly != 0 || $customeronly != 0 ]]; then
      Mef_Users_Post_Process $OUTPUTFILE $IBMONLY $CUSTOMERONLY
  fi
}

function collect_LDAP_users_aix
{
  tmp_user_file="/tmp/ldapuser_tmp"
      
  if [[ $OS = "AIX" ]]; then
    attr=`lsuser -R LDAP ALL > $tmp_user_file`;
    while read nextline; do
      logDebug "collect_LDAP_users_aix: read $nextline"
      
      if echo "$nextline" | grep "registry=LDAP.*SYSTEM=.*LDAP" >/dev/null; then
        username=`echo $nextline | awk '{ print $1 }'`
        testvar=1
        AStore LDAP_users ${username} "$testvar"
        logDebug "collect_LDAP_users_aix: $username added "
      fi
    done < $tmp_user_file
    
    `rm -f $tmp_user_file`
  fi  
}

function getdomainname
{
  domain=""
  if [ -r /etc/resolv.conf ]; then  
    domain=`awk '/^domain/ {print $2}' /etc/resolv.conf`
  fi
  
  if [[ $domain = "" ]]; then
    domain=`nslookup $HOST | awk '/^Name:/{print $2}'`
    domain=${domain#*\.}
  fi  
  echo "$domain"
}
#####################################################################################################
# GSA
#####################################################################################################
function checkGSAconfig
{
  flag=0
  METHODCFG="/usr/lib/security/methods.cfg"
  
  logDebug "checkGSAconfig: check configuration"
  
  if [ $OS = "AIX" ]; then
      if [[ ! -z `cat $SECUSER|grep SYSTEM|grep -v '*'|grep GSA` && ! -z `cat $METHODCFG|grep -v ^*|grep GSA` ]]; then
        flag=1
      fi  
  fi  
  
  if [ $OS = "Linux" ]; then
    if [[ (! -z `cat /etc/nsswitch.conf|grep ldap` && ! -z `cat /etc/pam.d/*|grep gsa`) || ! -z `grep gsa /etc/security/*`  ]]; then
      flag=1
    fi  
  fi
  
  logDebug "checkGSAconfig: return value is $flag"
  
  return $flag
  
}

function GSALDAP
{
  logDebug "GSALDAP: get LDAP server address"
  
  LDAPSVR=""
  if [ -r $GSACONF ]; then
    LDAPSVR=`awk '/^cellname/ {print $2}' $GSACONF |cut -d , -f1|tail -1`
    if [[ $LDAPSVR = "" ]]; then
      LDAPSVR=`awk '/^ldaphost/ {print $2}' $GSACONF |cut -d , -f1|tail -1`
    fi  
  
    if [[ $LDAPSVR = "" ]]; then
      LDAPSVR=`awk '/^host/ {print $2}' $GSACONF |cut -d , -f1|tail -1`
    fi  
  fi
  
  if [[ $OS = "Linux" && $LDAPSVR = "" ]]; then
    LDAPSVR=`awk '/^host/ {print $2}' $LDAPCONF |cut -d , -f1|tail -1`
  fi
    
  logDebug "GSALDAP: LDAP server address is $LDAPSVR"
}

function getLDAPBASE
{
  logDebug "getLDAPBASE: start"
  
  gsabase=""
  
  if [ -r $GSACONF ]; then
    gsabase=`cat $GSACONF|egrep '^base|^ldapbase'|awk '{print $2}'`
  fi
  
  if [[ $gsabase = "" ]]; then
    gsabase=`cat $LDAPCONF|egrep '^base|^ldapbase'|awk '{print $2}'`
  fi
    
  if [[ $gsabase != "" ]]; then
    PEOPLEBASE="ou=People,$gsabase"
    GROUPBASE="ou=Group,$gsabase"
  fi
  
  if [[ $OS = "Linux" && $gsabase = "" ]]; then
    PEOPLEBASE=`awk '/^nss_base_passwd/ {print $2}' $LDAPCONF`
    PEOPLEBASE=${PEOPLEBASE%%*\?}
    GROUPBASE=`awk '/^nss_base_group/ {print $2}' $LDAPCONF`
    GROUPBASE=${GROUPBASE%%*\?}
  fi
  #not checked
  logDebug "getLDAPBASE:$PEOPLEBASE : $GROUPBASE"
}

function extractSudoUsersGroups
{
 declare tmp_sudo_file="/tmp/sudoersfile.tmp"
 `rm -f $tmp_sudo_file`

 preparsesudoers $SUDOERFILE $tmp_sudo_file
 
 DATA=`egrep -v "^#" $tmp_sudo_file| sed 's/^\+\(.*\)/LDAP\/\1/g' | sed 's/^[    ]*//;s/[	 ]*$//'|sed -e :a -e '/\\\\$/N; s/\\\\\n//; ta'|sed 's/	/ /g'|tr -s '[:space:]'|sed '/^$/d'|sed 's/, /,/g'|sed 's/ ,/,/g'|sed 's/ =/=/g'|sed 's/= /=/g'>$TMPFILE` 

 while read nextline; do
   declare -a tokens=(`echo $nextline`)
   case ${tokens[0]} in
     Cmnd_Alias ) continue ;;
     Runas_Alias )continue ;;
     Defaults )continue ;;
     ALL ) continue ;;
     Host_Alias ) continue ;;
     User_Alias )
     declare -a UAtokens=(`echo $nextline|sed 's/=/ /g'`)
     tokenlist=${UAtokens[2]}
       
     IFS=,;for nexttoken in ${tokenlist}
     do
      if echo "$nexttoken" | grep "^%" >/dev/null; then
        group=`echo ${nexttoken}|tr -d %`
        group=`trim "$group"`
        if [[ $gsagrouplist = "" ]]; then
          gsagrouplist="$group"
        else  
         if echo "$gsagrouplist" | grep -v "$group" > /dev/null; then
           gsagrouplist=$gsagrouplist",$group"
         fi  
        fi  
      fi  
     done   
     IFS=" "
      continue 
     ;;
     * )
     for nexttoken in ${nextline}
     do
       tokenlist=${tokens[0]}
       IFS=,;for nexttoken in ${tokenlist}
       do
        if echo "$nexttoken" | grep "^%" >/dev/null; then
          group=`echo ${nexttoken}|tr -d %`
          group=`trim "$group"`
          if [[ $gsagrouplist = "" ]]; then
            gsagrouplist="$group"
          else  
           if echo "$gsagrouplist" | grep -v "$group" > /dev/null; then
             gsagrouplist=$gsagrouplist",$group"
           fi  
          fi  
        fi  
       done   
     done
     IFS=" "
 ;;
 esac
 done < $TMPFILE
 
 `rm -f $tmp_sudo_file`
}

function getGSAgroup
{
  grouplist=$1
  logDebug "getGSAgroup: starting: $grouplist"
  `echo "" > $LDAPGROUP&& rm $LDAPGROUP`
  oIFS=$IFS
  IFS=,
  declare -a groupz=(`echo $grouplist`)
  IFS=$oIFS
  for gsagroup in ${groupz[@]}
  do
    logDebug "getGSAgroup: get gsagroup $gsagroup information"
    `$LDAPCMD -LLL -h $LDAPSVR -b "$GROUPBASE" cn="$gsagroup" cn gidNumber memberUid > /tmp/gsatmp.tmp`
    if [[ $? -ne 0 ]]; then
      logAbort "accessing LDAP server ($?)"
    fi
    
    gidNumber=""
    memberUid=""
    
    while read line; do
      
      logDebug "getGSAgroup: line=$line"
      
      if echo "$line" | grep -i "gidNumber:" > /dev/null; then
        gidNumber=$(echo "$line" | sed -n 's/^gidNumber: \(.*\)/\1/p')
        logDebug "getGSAgroup: gidNumber=$gidNumber"
        continue
      fi

      if echo "$line" | grep -i "memberUid:" > /dev/null; then
        tempUid=$(echo "$line" | sed -n 's/^memberUid: \(.*\)/\1/p')
        logDebug "getGSAgroup Add ID $tempUid" 
        testvar=1
        AStore MEMBERS ${tempUid} "$testvar"
        if [[ $memberUid = "" ]]; then
          memberUid="$tempUid"
        else
          memberUid="$memberUid,$tempUid"
        fi
        logDebug "getGSAgroup: memberUid=$memberUid"
        continue
      fi
    done </tmp/gsatmp.tmp
    
    echo "$gsagroup:!:$gidNumber:$memberUid" >> $LDAPGROUP
    logDebug "getGSAgroup:groupfile->$gsagroup:!:$gidNumber:$memberUid"
  done
  #IFS=$oIFS
  `rm /tmp/gsatmp.tmp`
}

function getGroupGID
{
  group=$1
  `$LDAPCMD -LLL -h $LDAPSVR -b "$GROUPBASE" cn="$group" gidNumber > /tmp/getGroupGID.tmp`
  if [[ $? -ne 0 ]]; then
    logAbort "accessing LDAP server ($?)"
  fi
  tempGid=""
  while read str
  do
    if echo "$str" | grep -i "gidNumber:" > /dev/null; then
      tempGid=$(echo "$str" | sed -n 's/^gidNumber: \(.*\)/\1/p')
      break
    fi
  done < /tmp/getGroupGID.tmp
  `rm /tmp/getGroupGID.tmp`
  echo "$tempGid"  
}

function getAdditionalGroup
{
  AGetAll MEMBERS keys
  for gsauid in ${keys[@]}
  do
    logDebug "getAdditionalGroup: uid=$gsauid"
    `$LDAPCMD -LLL -h $LDAPSVR -b "$GROUPBASE" memberUid="$gsauid" cn > /tmp/getAdditionalGroup.tmp`
    while read line
    do
      logDebug "getAdditionalGroup:$line"
      if echo "$line" | grep -i "cn:" > /dev/null; then
        gsagroup=$(echo "$line" | sed -n 's/^cn:\(.*\)/\1/p')
        gsagroup=`trim "$gsagroup"`
        gidNumber=`getGroupGID "$gsagroup"`
        logDebug "getAdditionalGroup: gsagroup=$gsagroup, gidNumber=$gidNumber"
        echo "$gsagroup:!:$gidNumber:$gsauid" >> $LDAPGROUP
        continue
      fi
    done < /tmp/getAdditionalGroup.tmp
  done
  `rm /tmp/getAdditionalGroup.tmp`
}

function getGSAuser
{
  logDebug "getGSAuser: starting"
  
  AGetAll MEMBERS keys 
    
  for gsauid in ${keys[@]}
  do  
    logDebug "getGSAuser: gsauid=$gsauid"
    `$LDAPCMD -LLL -h $LDAPSVR -b "$PEOPLEBASE" uid="$gsauid" uniqueIdentifier cn > /tmp/getGSAuser.tmp`
    
    uniqueIdentifier="";
    cn="";

    while read line
    do    
      logDebug "getGSAuser: line=$line"
      if echo "$line" | grep -i "uniqueIdentifier:" > /dev/null; then
        uniqueIdentifier=$(echo "$line" | sed -n 's/^uniqueIdentifier: \(.*\)/\1/p')
        logDebug "getGSAuser: uniqueIdentifier=$uniqueIdentifier"
        continue
      fi
      
      if echo "$line" | grep -i "cn:" > /dev/null; then
        cn=$(echo "$line" | sed -n 's/^cn: \(.*\)/\1/p')
        cn=`trim "$cn"`
        logDebug "getGSAuser: cn=$cn"
      fi  
    done < /tmp/getGSAuser.tmp
    
    if [[ $uniqueIdentifier = "" ]]; then
      continue
    fi  
    
    CC=$(echo $uniqueIdentifier|cut -c7-9)
    SN=$(echo $uniqueIdentifier|cut -c1-6)
    
    IDs=`id -u $gsauid`
    IDs=`trim "$IDs"`
    GROUPID=`id -g $gsauid`
    GROUPID=`trim "$GROUPID"`
    
    echo "$gsauid:!:$IDs:$GROUPID:$CC/I/$SN/IBM/$cn-GSA::" >> $LDAPPASSWD
    logDebug "getGSAuser:passwd->$gsauid:!:$IDs:$GROUPID:$CC/I/$SN/IBM/$cn-GSA::"
  done
  `rm /tmp/getGSAuser.tmp`
}


function collectGSAusers
{
  logDebug "collectGSAusers: started"
  
  GSALDAP
  if [[ $LDAPSVR = "" ]]; then 
    logAbort "LDAP server address not found"
  fi 
  
  getLDAPBASE
  
  if [ -r $GSACONF ]; then
    gsagrouplist=`cat $GSACONF | grep "^gsagroupallow" | sed "s/gsagroupallow //g"|sed "s/,/ /g"`
    logDebug "collectGSAusers: gsagrouplist = $gsagrouplist"
  fi  
  
  if [[ $OS = "Linux" && $gsagrouplist = "" ]]; then
    gsagrouplist=`cat $LDAPCONF|grep "^gsagroupallow" |sed "s/gsagroupallow //g"|sed "s/,/ /g"`
  fi
  
  if [[ $gsagrouplist = "" ]]; then
    for groupid in ${keys[@]}
    do
      if [[ $gsagrouplist = "" ]]; then
        gsagrouplist="$groupid"
      else  
        gsagrouplist=$gsagrouplist",$groupid"
      fi  
    done        
    extractSudoUsersGroups
  fi
  
  logDebug "collectGSAusers: gsagrouplist = $gsagrouplist"

  if [[ $gsagrouplist = "" ]]; then
    logAbort "Can't get GSA group list"
  fi

  getGSAgroup "$gsagrouplist"
  getGSAuser
  
  if [[ $gsagrouplist != "" ]]; then
    getAdditionalGroup 
  fi
  
  logDebug "collectGSAusers: finished"
}  
#####################################################################################################
## MAIN
#####################################################################################################
OS=`uname -a|cut -d" " -f1`
PERL=`which perl`

if [[ $OS = "Tru64" || $OS = "OSF1" ]]; then
CMD_ENV_OLD=$CMD_ENV  
export CMD_ENV=xpg4
fi
SCRIPTNAME=$0
CKSUM=`cksum $SCRIPTNAME | awk '{ print $1 }'`
if [[ $OS = "Tru64" || $OS = "OSF1" ]]; then
export CMD_ENV=$CMD_ENV_OLD
fi

logHeader
 
date=`date +%d%b%Y`
DATE=`echo $date | tr -d ' ' | tr "[:lower:]" "[:upper:]"`
myAUDITDATE=`date +%Y-%m-%d-%H.%M.%S`
findSudoersFile
PASSWDFILE="/etc/passwd"
GROUPFILE="/etc/group"

HOST=`hostname`
LONG_HOST_NAME=$HOST
HOST=${LONG_HOST_NAME%%.*}
HOSTNAME=$HOST

if [ $HOST = $LONG_HOST_NAME ]; then
DOMAINNAME=`getdomainname`
if [[ $DOMAINNAME != "" ]]; then
  LONG_HOST_NAME=$HOST".$DOMAINNAME"
  fi
fi  

ENABLEFQDN=1
TMPFILE="/tmp/iam_extract_global.tmp"       #4.2 Updated to keep tmpfile in /tmp
CUSTOMER="IBM"
OUTPUTFILE="/tmp/$CUSTOMER""_""$DATE""_""$HOST.mef"

USERCC="897"
NETGROUP=0
LDAP_NETGOUP_TMP="/tmp/ldap_netgroup.tmp"

GSACONF="/usr/gsa/etc/gsa.conf"
LDAPCONF="/etc/ldap.conf"

uname=`uname`
export uname

if [ -f /bin/sudo ]; then
  SUDOCMD="/bin/sudo"
  SUDOVER=`$SUDOCMD -V|grep -i 'Sudo version'|cut -f3 -d" "`
  logInfo "SUDO Version: $SUDOVER"
elif [ -f /usr/bin/sudo ]; then
  SUDOCMD="/usr/bin/sudo"
  SUDOVER=`$SUDOCMD -V|grep -i 'Sudo version'|cut -f3 -d" "`
  logInfo "SUDO Version: $SUDOVER"
elif [ -f /usr/local/bin/sudo ]; then
  SUDOCMD="/usr/local/bin/sudo"
  SUDOVER=`$SUDOCMD -V|grep -i 'Sudo version'|cut -f3 -d" "`
  logInfo "SUDO Version: $SUDOVER"
elif [ -f /usr/local/sbin/sudo ]; then
  SUDOCMD="/usr/local/sbin/sudo"
  SUDOVER=`$SUDOCMD -V|grep -i 'Sudo version'|cut -f3 -d" "`
  logInfo "SUDO Version: $SUDOVER"
else
  SUDOVER="NotAvailable"
  logMsg "WARNING" "unable to get Sudo Version:$SUDOVER."
  EXIT_CODE=1
fi

if [[ $OS = "AIX" ]]; then
  SECUSER="/etc/security/user"
  SPASSWD="/etc/security/passwd"
elif [[ $OS = "HP-UX" ]]; then
  SECUSER=""
  SPASSWD="/etc/shadow"
elif [[ $OS = "SunOS" ]]; then
  SECUSER=""
  SPASSWD="/etc/shadow"
elif [[ $OS = "Linux" ]]; then
  SECUSER=""
  SPASSWD="/etc/shadow"
elif [[ $OS = "Tru64" || $OS = "OSF1" ]]; then
  SECUSER=""
  SPASSWD="/etc/shadow"
else
  SECUSER=""
  SPASSWD="/etc/shadow"
fi

SCMFORMAT="0"
MEF2FORMAT="0"
NEWOUTPUTFILE=""
NIS=0
LDAP=0
ldap_tmp="/tmp/iam_temp"
ldap_tmp1="/tmp/iam_temp1"
NOAUTOLDAP=0
CUSTOMERONLY=0
IBMONLY=0
OWNER=""
DLLD=0
NOGSA=0
LDAPFILE=""
LDAPBASEGROUP=""
LDAPGROUPOBJCLASS=""
LDAPADDITIONAL=""
NISPLUSDIR=""

IS_ADMIN_ENT_ACC=0

c=0
for var in "$@"
do
  argums[$c]=$var   
  let "c+=1"
done
c=0
while ((c<${#argums[*]}))
do
  case "${argums[$c]}" in
  -f ) 
    let "c+=1"
    SUDOERFILE="${argums[$c]}"
    KNOWPAR=$(echo "$KNOWPAR -f")
    ;;
  -g ) 
    let "c+=1"
    GROUPFILE="${argums[$c]}"
    NOAUTOLDAP=1
    KNOWPAR=$(echo "$KNOWPAR -g")
    ;;
  -p ) 
    let "c+=1"
    PASSWDFILE="${argums[$c]}"
    NOAUTOLDAP=1
    KNOWPAR=$(echo "$KNOWPAR -p")
    ;;
  -r ) 
    let "c+=1"
    NEWOUTPUTFILE="${argums[$c]}"
    KNOWPAR=$(echo "$KNOWPAR -r")
    ;;
  -c ) 
    let "c+=1"
    CUSTOMER="${argums[$c]}"
    KNOWPAR=$(echo "$KNOWPAR -c")
    ;;
  -o ) 
    let "c+=1"
    OS="${argums[$c]}"
    KNOWPAR=$(echo "$KNOWPAR -o")
    ;;
  -s ) 
    let "c+=1"
    SPASSWD="${argums[$c]}"
    KNOWPAR=$(echo "$KNOWPAR -s")
    ;;
  -u ) 
    let "c+=1"
    SECUSER="${argums[$c]}"
    KNOWPAR=$(echo "$KNOWPAR -u")
    ;;
  -m ) 
    let "c+=1"
    LONG_HOST_NAME="${argums[$c]}"
    HOSTNAME=$LONG_HOST_NAME
    HOST=${LONG_HOST_NAME%%.*}
    ENABLEFQDN=0
    KNOWPAR=$(echo "$KNOWPAR -m")
    ;;
  -d ) 
    DEBUG="1"
    KNOWPAR=$(echo "$KNOWPAR -d")
    ;;
  -S ) 
    SCMFORMAT="1"
    KNOWPAR=$(echo "$KNOWPAR -S")
    ;;
  -M ) 
    MEF2FORMAT="1"
    KNOWPAR=$(echo "$KNOWPAR -M")
    ;;
  -P ) 
    let "c+=1"
    PRIVFILE="${argums[$c]}"
    KNOWPAR=$(echo "$KNOWPAR -p")
    ;;
  -n )                     #4.3 Custom Signature
    let "c+=1"
    SIG=`echo "${argums[$c]}" | tr "[:lower:]" "[:upper:]"`
    KNOWPAR=$(echo "$KNOWPAR -n")
    ;;
   -N )    
    let "c1=$c+1"
    if [ $c1 -lt ${#argums[*]} ]; then
      if echo "${argums[$c1]}" | grep  "^-" >/dev/null; then
        NISPLUSDIR=""
      else
        NISPLUSDIR=".${argums[$c1]}"
        c=$c1
      fi  
    fi  
    NISPLUSDIR=""
    NIS=1
    NOAUTOLDAP=1
    KNOWPAR=$(echo "$KNOWPAR -N")
    ;;
  -L )
    LDAP=1
    NOAUTOLDAP=1
    let "c+=1"
    if echo "${argums[$c]}" | grep  "\:" >/dev/null; then
      LDAPARG="${argums[$c]}"
      LDAPSVR=`echo "${argums[$c]}" | awk -F: '{ print $1 }'`
      LDAPPORT=`echo "${argums[$c]}" | awk -F: '{ print $2 }'`
      LDAPBASE=`echo "${argums[$c]}" | awk -F: '{ print $3 }'`
    else
     logAbort "-L ServerName/IP:port:BaseDN\neg: iam_extract_ibm.ksh -L 127.0.0.1:389:DC=IBM,DC=COM"
    fi
    KNOWPAR=$(echo "$KNOWPAR -L")
    ;;
  -l )
    let "c+=1"
    LDAP=1
    NOAUTOLDAP=1
    LDAPFILE="${argums[$c]}"
    KNOWPAR=$(echo "$KNOWPAR -l")
    ;;
  -K )                      #4.5 Added NIS and LDAP 
    CUSTOMERONLY=1
    KNOWPAR=$(echo "$KNOWPAR -K")
    ;;
  -I )                      #4.5 Added NIS and LDAP 
    IBMONLY=1
    KNOWPAR=$(echo "$KNOWPAR -I")
    ;;
  -h|-help ) 
    echo
    echo "Version: $VERSION"
    echo "USAGE: iam_extract_global.ksh [-f sudoers_file] [-r results_file] [-p passwd_file]"
    echo "                           [-g group_file] [-c customer] [-m hostname]" 
    echo "                           [-o ostype] [-s shadowfile] -u [secuserfile]" 
    echo "                           [-S] [-M] [-P privfile] [-n TSCM|SCR|TCM|FUS]" 
    echo "                           [-L <LDAP SERVER IP:Port:BASE DN>] [-N [<directory>]] [-q] [-a]" 
    echo "                           [-K] [-I] [-O <owner>] [-D] [-G] [-d]" 
    echo "                           [-l <ldap_cfg_file>]" 
    echo
    echo "  -S   Change output file format to scm9, instead of mef3"
    echo "  -M   Change output file format to mef2, instead of mef3"
    echo "  -q   Use fully qualified domain name(FQDN)"
    echo "  -a   Fetch only local user IDs (Linux, Solaris)"
    echo "  -K   Flag to indicate if only Customer userID's should be written to the output"
    echo "  -I   Flag to indicate if only IBM userID's should be written to the output"
    echo "  -G   Disable GSA"
    echo "  -d   Debug mode"    
    echo
    echo " Defaults:"
    echo "     CUSTOMER: $CUSTOMER"
    echo "   SUDOERFILE: $SUDOERFILE"
    echo "   PASSWDFILE: $PASSWDFILE"
    echo "    GROUPFILE: $GROUPFILE"
    echo "  RESULTSFILE: $OUTPUTFILE"
    echo "   SHADOWFILE: $SPASSWD"
    echo "  SECUSERFILE: $SECUSER"
    echo "           OS: $OS(AIX|HP-UX|SunOS|Linux|Tru64"
    echo "     HOSTNAME: $HOST"
    echo "        CKSUM: $CKSUM"
    echo
    echo " Output is mef format including SUDO privilege data."
    echo " User 'state' (enabled/disabled) is extracted if possible."
    exit 9
    ;;
  -q )                   
    FQDN=1
    KNOWPAR=$(echo "$KNOWPAR -q")
    ;;
  -a )
    NOAUTOLDAP=1
    KNOWPAR=$(echo "$KNOWPAR -a")
    ;;
  -O )        
    let "c+=1"           
    OWNER="${argums[$c]}"
    KNOWPAR=$(echo "$KNOWPAR -O")
    ;;
  -D )                   
    DLLD=1
    KNOWPAR=$(echo "$KNOWPAR -D")
    ;;
  -G )                   
    NOGSA=1
    KNOWPAR=$(echo "$KNOWPAR -G")
    ;;
   * )
    if [ $c -lt ${#argums[*]} ]; then
      UNKNOWPAR=$(echo "$UNKNOWPAR ${argums[$c]}")
    fi
   ;; 
  esac
  let "c+=1"
done

if [[ $OS = "AIX" ]]; then
  logInfo "Found AIX"
  PRIVUSERS='^root$|^daemon$|^bin$|^sys$|^adm$|^uucp$|^nuucp$|^lpd$|^imnadm$|^ipsec$|^ldap$|^lp$|^snapp$|^invscout$|^nobody$|^notes$'
  PRIVGROUPS='^system$|^security$|^bin$|^sys$|^adm$|^uucp$|^mail$|^printq$|^cron$|^audit$|^shutdown$|^ecs$|^imnadm$|^ipsec$|^ldap$|^lp$|^haemrm$|^snapp$|^hacmp$|^notes$|^mqm$|^dba$|^sapsys$|^db2iadm1$|^db2admin$|^sudo$'
elif [[ $OS = "HP-UX" ]]; then
  logInfo "Found HP-UX"
  PRIVUSERS='^root$|^daemon$|^bin$|^sys$|^adm$|^uucp$|^lp$|^nuucp$|^hpdb$|^imnadm$|^nobody$|^notes$'
  PRIVGROUPS='^root$|^other$|^bin$|^sys$|^adm$|^daemon$|^mail$|^lp$|^tty$|^nuucp$|^nogroup$|^imnadm$|^mqm$|^dba$|^sapsys$|^db2iadm1$|^db2admin$|^sudo$|^notes$'
elif [[ $OS = "SunOS" || $OS = "Solaris" ]]; then
  logInfo "Found SunOS"
  PRIVUSERS='^root$|^daemon$|^bin$|^sys$|^adm$|^uucp$|^nuucp$|^imnadm$|^lp$|^smmsp$|^listen$'
  PRIVGROUPS='^system$|^security$|^bin$|^sys$|^uucp$|^mail$|^imnadm$|^lp$|^root$|^other$|^adm$|^tty$|^nuucp$|^daemon$|^sysadmin$|^smmsp$|^nobody$|^notes$|^mqm$|^dba$|^sapsys$|^db2iadm1$|^db2admin$|^sudo$'
elif [[ $OS = "Linux" ]]; then
  logInfo "Found Linux"
  PRIVUSERS='^root$|^daemon$|^bin$|^sys$|^nobody$|^notes$'
  PRIVGROUPS='^notes$|^mqm$|^dba$|^sapsys$|^db2iadm1$|^db2admin$|^sudo$|^wheel$'
elif [[ $OS = "Tru64" || $OS = "OSF1" ]]; then
  logInfo "Found Tru64"
  PRIVUSERS='^adm$|^auth$|^bin$|^cron$|^daemon$|^inmadm$|^lp$|^nuucp$|^ris$|^root$|^sys$|^tcb$|^uucp$|^uucpa$|^wnn$'
  PRIVGROUPS='^adm$|^auth$|^backup$|^bin$|^cron$|^daemon$|^inmadm$|^kmem$|^lp$|^lpr$|^mail$|^mem$|^news$|^operator$|^opr$|^ris$|^sec$|^sysadmin$|^system$|^tape$|^tcb$|^terminal$|^tty$|^users$|^uucp$'
else
  logInfo "Found Unknown OS"
  PRIVUSERS='^root$|^daemon$|^bin$|^sys$|^adm$|^uucp$|^nuucp$|^lpd$|^imnadm$|^ipsec$|^ldap$|^lp$|^snapp$|^invscout$|^nobody$|^notes$'
  PRIVGROUPS='^1bmadmin$|^adm$|^audit$|^bin$|^cron$|^daemon$|^db2admin$|^db2iadm1$|^dba$|^ecs$|^hacmp$|^haemrm$|^ibmadmin$|^imnadm$|^ipsec$|^ldap$|^lp$|^mail$|^mqm$|^nobody$|^nogroup$|^notes$|^nuucp$|^other$|^printq$|^root$|^sapsys$|^security$|^shutdown$|^smmsp$|^snapp$|^suroot$|^sys$|^sysadm$|^system$|^tty$|^uucp$|^wheel$'
fi

if [[ $ENABLEFQDN -eq 1 && $FQDN -eq 1 ]]; then
  HOSTNAME=$LONG_HOST_NAME
fi

logDebug "init: host $HOST:$LONG_HOST_NAME"

if [[ $NEWOUTPUTFILE != "" ]]; then
  if echo "$NEWOUTPUTFILE" | grep "/" > /dev/null; then
    OUTPUTFILE=$NEWOUTPUTFILE
  else
    OUTPUTFILE="/tmp/$NEWOUTPUTFILE"
  fi
else
  if [[ $SCMFORMAT -eq 1 ]]; then
    OUTPUTFILE="/tmp/$CUSTOMER""_""$DATE""_""$HOSTNAME.scm9"
  elif [[ $MEF2FORMAT -eq 1 ]]; then
    OUTPUTFILE="/tmp/$CUSTOMER""_""$DATE""_""$HOSTNAME.mef"
  else
    OUTPUTFILE="/tmp/$CUSTOMER""_""$DATE""_""$HOSTNAME.mef3"
  fi
fi

if [[ $PRIVFILE != "" ]]; then
  if [ -r $PRIVFILE ]; then
    logDebug "Reading PRIVFILE: $PRIVFILE"
    while read line; do
      matched=`echo $line|egrep -v '^\s*$'|wc -l`
      if [[ $matched -gt 0 ]]; then
        logDebug "Found Additional Priv group: ----$line""----"
        PRIVGROUPS=$PRIVGROUPS"|^"$line'$'
      fi

    done < $PRIVFILE
  else
    logMsg "WARNING" "unable to read PRIVFILE:PRIVFILE."
    EXIT_CODE=1
  fi
fi

logDebug "PRIVSUSERS: $PRIVUSERS"
logDebug "PRIVSGROUPS: $PRIVGROUPS"

SEC_READABLE=1
if [ ! -r $SPASSWD ]; then
  logMsg "WARNING" "unable to read SPASSWD:$SPASSWD. Account state may be missing from extract"
  SEC_READABLE=0
  EXIT_CODE=1
fi

if [[ $OS = "AIX" ]]; then
  if [ ! -r $SECUSER ]; then
    logMsg "WARNING" "unable to read SECUSER:$SECUSER. Account state may be missing from extract"
    SEC_READABLE=0
    EXIT_CODE=1
  fi
fi

TCB_READABLE=0
if [[ $OS = "HP-UX" ]]; then
  #echo "CHECKING: /usr/lbin/getprpw."
  if [ ! -x /usr/lbin/getprpw ]; then
    logMsg "WARNING" "unable to execute /usr/lbin/getprpw. Account state may be missing from extract"
    TCB_READABLE=0
    EXIT_CODE=1
  else
    TCB_READABLE=1
  fi
fi

logDebug "TCB_READABLE: $TCB_READABLE"

if [ $SUDOERFILE = "/dev/null" ]; then
  logMsg "WARNING" "unable to find sudoers file.  Account SUDO privileges will be missing from extract"
  EXIT_CODE=1
elif [ ! -r $SUDOERFILE ]; then
  logMsg "WARNING" "unable to read SUDOERFILE:$SUDOERFILE file.  Account SUDO privileges will be missing from extract"
  EXIT_CODE=1
fi

if [ ! -r $GROUPFILE ]; then
logAbort "unable to read $GROUPFILE"
fi

if [ ! -r $PASSWDFILE ]; then
logAbort "unable to read $PASSWDFILE"
fi

`echo "" > $OUTPUTFILE&& rm $OUTPUTFILE` 
if [[ $? -ne 0 ]]; then
  logAbort "unable to open OUTPUTFILE:$OUTPUTFILE"
fi

`echo "" > $TMPFILE&& rm $TMPFILE` 
if [[ $? -ne 0 ]]; then
  logAbort "unable to open $TMPFILE"
fi

AStore MNames "Jul" "1"
AStore MNames "Aug" "2"
AStore MNames "Sep" "3"
AStore MNames "Oct" "4"
AStore MNames "Nov" "5"
AStore MNames "Dec" "6"
AStore MNames "Jan" "7"
AStore MNames "Feb" "8"
AStore MNames "Mar" "9"
AStore MNames "Apr" "10"
AStore MNames "May" "11"
AStore MNames "Jun" "12"

errorCount=0

# call the function for check on the possibility usages of system functions for extracts userIDs from services LDAP, NIS, NIS+
IsAdminEntAccessible
logInfo "Checking on the admin ent accessible => '$IS_ADMIN_ENT_ACC'"

ADMENTPASSWD="/tmp/adment_passwd"
ADMENTGROUP="/tmp/adment_group"

LDAPPASSWD="/tmp/ldappasswd"
LDAPGROUP="/tmp/ldapgroup"

logPostHeader $0

PROCESSNIS=0
PROCESSLDAP=0

if [[ $LDAP -eq 1 ]]; then
  if [[ $OS = "AIX" || $OS = "SunOS" ]]; then
      LDAPCMD="ldapsearch"
      if [[ $OS = "AIX" ]]; then
        attr=`$LDAPCMD -? 2>/dev/null`
        if [[ $? -ne 0 ]]; then
          LDAPCMD="idsldapsearch"
        fi
      fi  
  else
      LDAPCMD="ldapsearch -x"
  fi
  
  if [[ $LDAPFILE != "" ]]; then
    LDAPSVR=`awk -F: '/^LDAPSVR:/ {print $2}' $LDAPFILE`
    LDAPBASE=`awk -F: '/^LDAPBASEPASSWD:/ {print $2}' $LDAPFILE`
    LDAPBASEGROUP=`awk  -F: '/^LDAPBASEGROUP:/ {print $2}' $LDAPFILE`
    LDAPPORT=`awk -F: '/^LDAPPORT:/ {print $2}' $LDAPFILE`
    LDAPGROUPOBJCLASS=`awk -F: '/^LDAPGROUPOBJCLASS:/ {print $2}' $LDAPFILE`
    LDAPADDITIONAL=`awk -F: '/^LDAPADDITIONAL:/ {print $2}' $LDAPFILE`
    logDebug "\nLDAPFILE:$LDAPFILE\nLDAPSVR:$LDAPSVR\nLDAPBASEPASSWD:$LDAPBASE\nLDAPBASEGROUP:$LDAPBASEGROUP\nLDAPPORT:$LDAPPORT\nLDAPGROUPOBJCLASS:$LDAPGROUPOBJCLASS\nLDAPADDITIONAL:$LDAPADDITIONAL"
    if [[ $LDAPSVR = "" || $LDAPBASE = "" || $LDAPPORT = "" || $LDAPGROUPOBJCLASS = "" || $LDAPBASEGROUP = "" ]]; then
      logAbort "Invalid $LDAPFILE, exiting"
    fi
  fi
fi

if [[ NOGSA -eq 0 ]]; then
  checkGSAconfig
  if [[ $? -eq 1 ]]; then
    logInfo "Start GSA processing"
    LDAPPASSWD="/tmp/ldappasswd"
    LDAPGROUP="/tmp/ldapgroup"
    if [[ $OS = "AIX" || $OS = "SunOS" ]]; then
      LDAPCMD="/usr/gsa/bin/ldapsearch"
    else
      LDAPCMD="/usr/bin/ldapsearch -x"
    fi    
    NOAUTOLDAP=1
    collectGSAusers
    PROCESSLDAP=1
    LDAP=1
    Parse_User
    Parse_Grp
    
    LDAP=0
    Parse_Grp
    
    if [[ $OS != "AIX" ]]; then
      LDAP=0
      PROCESSLDAP=0
      Parse_Grp
      LDAP=1
      PROCESSLDAP=1
    fi      
    LDAP=1
    logInfo "Parse Sudo"    
    Parse_Sudo        
    logInfo "Finish GSA processing"    
    report
    
    LDAP=0
    
    #cleaning of hashes
    AUnset primaryGroupUsers
    AUnset PasswdUser
    AUnset groupGIDName
    AUnset ALLGroupUsers
    AUnset AllUserGroups
    AUnset privUserGroups
    AUnset Ullogon
    AUnset sudoUsers
    AUnset sudoGroups
    AUnset sudoUserGroups
    AUnset aliasUsers
    AUnset validHostAlias
    AUnset Netgrplist
    
    if [ -a $LDAPPASSWD ]; then
      `rm $LDAPPASSWD`
    fi

    if [ -a $LDAPGROUP ]; then
      `rm $LDAPGROUP`
    fi
    
  fi
fi

if [[ $NIS -eq 1 ]]; then
    NISPLUS=0
    check_nisplus
    if  [[ $? -eq 1 ]]; then
      NISPLUS=1
    fi
    logInfo "Start NIS processing"
    if [[ NISPLUS -eq 1 ]]; then
      ret=`niscat passwd.org_dir$NISPLUSDIR > /tmp/nis_passwd`
      ret=`niscat group.org_dir > /tmp/nis_group`
    else
      ret=`ypcat passwd > /tmp/nis_passwd`
      ret=`ypcat group > /tmp/nis_group`
    fi
    
    if [[ $? -ne 0 ]]; then
       logAbort "Unable to accessing NIS server"
    fi
    PROCESSNIS=1
    NISPASSWD="/tmp/nis_passwd"
    NISGROUP="/tmp/nis_group"
    logInfo "Parse NIS users"    
    Parse_User
    logInfo "Parse NIS groups"        
    Parse_Grp
    
    NIS=0
    PROCESSNIS=0
    Parse_Grp
    NIS=1
    PROCESSNIS=1
    
    logInfo "Parse Sudo"    
    Parse_Sudo        # for NIS's accounts we must extract all data from SUDO-settings
    report
    rm -f /tmp/nis_passwd /tmp/nis_group
    
    #cleaning of hashes
    AUnset primaryGroupUsers
    AUnset PasswdUser
    AUnset groupGIDName
    AUnset ALLGroupUsers
    AUnset AllUserGroups
    AUnset privUserGroups
    AUnset Ullogon
    AUnset sudoUsers
    AUnset sudoGroups
    AUnset sudoUserGroups
    AUnset aliasUsers
    AUnset validHostAlias
    AUnset Netgrplist
    logInfo "Finish NIS processing"  
    PROCESSNIS=0  
fi

if [[ $IS_ADMIN_ENT_ACC -eq 1 && $NIS -eq 0 && $LDAP -eq 1 ]];
then
  checkforldappasswd
  if [[ $? -eq 1 ]]; then
    logInfo "Start LDAP processing"
    PROCESSLDAP=1
    logInfo "Parse LDAP users"
    collect_LDAP_users_aix
    process_LDAP_users
    parse_LDAP_grp
    Parse_User
    logInfo "Parse LDAP groups"     
    Parse_Grp
    
    if [[ $OS != "AIX" ]]; then
      LDAP=0
      PROCESSLDAP=0
      Parse_Grp
      LDAP=1
      PROCESSLDAP=1
    fi  
    
    logInfo "Parse Sudo"
    Parse_Sudo
    logInfo "Finish LDAP processing"
    report  
    AUnset primaryGroupUsers
    AUnset PasswdUser
    AUnset groupGIDName
    AUnset ALLGroupUsers
    AUnset AllUserGroups
    AUnset privUserGroups
    AUnset Ullogon
    AUnset sudoUsers
    AUnset sudoGroups
    AUnset sudoUserGroups
    AUnset aliasUsers
    AUnset validHostAlias
    AUnset Netgrplist    
  
    if [ -a $LDAPPASSWD ]; then
      `rm $LDAPPASSWD`
    fi

    if [ -a $LDAPGROUP ]; then
      `rm $LDAPGROUP`
    fi

    if [ -a $ldap_tmp ]; then 
      rm $ldap_tmp
    fi

    if [ -a $ldap_tmp1 ]; then 
      rm $ldap_tmp1
    fi
  fi      
fi  

PROCESSLDAP=0
Parse_User
Parse_Grp
Parse_Sudo
logInfo "Writing report"
report

if [ -a $LDAPPASSWD ]; then
   `rm $LDAPPASSWD`
fi

case $SIG in            #4.3 Custom Signature
TSCM )
  NOTAREALID="NOTaRealID-TSCM"
  ;;
SCR )
  NOTAREALID="NOTaRealID-SCR"
  ;;
TCM ) 
  NOTAREALID="NOTaRealID-TCM"
  ;;
FUS )
  NOTAREALID="NOTaRealID-FUS"
  ;;
*)
  NOTAREALID="NOTaRealID"
  ;;
esac

# adding dummy record
 if [[ $SCMFORMAT -eq 1 ]]; then 
    echo "$HOSTNAME\t$OS\t$myAUDITDATE\t$NOTAREALID\t000/V///$myAUDITDATE:FN=$0:VER=$VERSION:CKSUM=$CKSUM:SUDO=$SUDOVER\t1\t\t\t" >> $OUTPUTFILE
  elif [[ $MEF2FORMAT -eq 1 ]]; then
#MEF2 customer|system|account|userID convention data|group|state|l_logon|privilege
    echo "$CUSTOMER|$HOSTNAME|$NOTAREALID|000/V///$myAUDITDATE:FN=$0:VER=$VERSION:CKSUM=$CKSUM:SUDO=$SUDOVER||||" >> $OUTPUTFILE
  else
#MEF3 �customer|identifier type|server identifier/application identifier|OS name/Application name|account|UICMode|userID convention data|state|l_logon |group|privilege�
    echo "$CUSTOMER|S|$HOSTNAME|$OS|$NOTAREALID||000/V///$myAUDITDATE:FN=$0:VER=$VERSION:CKSUM=$CKSUM:SUDO=$SUDOVER||||" >> $OUTPUTFILE
 fi

DATA=`APrintAll sudoUsers " "> $TMPFILE`

#APrintAll sudoUsers " "| while read nextline; do
while read nextline; do
  #echo  "SUDOERS: $nextline "
  declare -a tokens=(`echo $nextline`)
  userid=${tokens[0]}
  logMsg "WARNING" "invalid user in $SUDOERFILE: $userid"
  EXIT_CODE=1
  #let errorCount=errorCount+1
done < $TMPFILE

if [ -a $TMPFILE ]; then
rm $TMPFILE
fi

if [ $errorCount -gt 0 ]; then
  logInfo "$errorCount errors encountered"
fi

Filter_mef3

if [[ $OWNER != "" ]]; then
    `chown $OWNER $OUTPUTFILE`
fi

logFooter
exit $EXIT_CODE